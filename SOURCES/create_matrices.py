# ---------------------------------------
# Load modules and required packages
# ---------------------------------------
import os
import antares
import numpy as np
from copy import copy

# ---------------------------------------
# The class dedicated to handling the
# creation of the linstab matrices
# ---------------------------------------
class CreateMatrices(object):

    def __init__(self, basename, equations):
        #
        self.baseflow = basename
        #
        self.ffem_location = os.popen('which FreeFem++').read().split('\n')[:-1][0]
        #
        self.ffem_file = os.popen('echo $GIFIE_HOME').read().split('\n')[:-1][0]+'/SOURCES/FreeFEM_FILES/'+equations+'.edp'
        #
        self.axi = False
        self.is2d = True
        self.is3d = False
        if 'axi' in equations:
            self.axi = True
            self.is2d = self.is3d = not self.axi
        elif '2d' in equations:
            pass
        elif '3d' in equations:
            self.is3d = True
            self.axi = self.is2d = not self.is3d
        self.isComp = False
        #
        if 'incompressible' in equations:
            self.isComp = False
        else:
            self.isComp = True

    def read_baseflow(self):
        #
        extension = self.baseflow.split('.')[-1]
        if extension == 'plt':
            extension = 'bin_tp'
        else:
            print( 'Fatal error: in CreateMatrices, no other extension than ''plt'' is handled for baseflow.')
            exit()
        #
        reader = antares.Reader(extension)
        reader['filename'] = self.baseflow
        self.base = reader.read()

    def write_ffem_mesh(self, boundaries=None, prefix='BASE/ffem-', ffemname='mesh.msh'):
        #
        points, connectivity, bnd_connectivity, points_labels = self._find_boundaries(ubounds=boundaries, prefix=prefix, suffix='.dat')
        if points_labels is None:
            self.nol = 1
        else:
            self.nol = len(np.unique(points_labels))
        #
        with open(prefix+ffemname,'w') as fid:
            fid.write('%d %d %d\n'%(points.shape[0],connectivity.shape[0],bnd_connectivity.shape[0]))
            for idx in range(points.shape[0]):
                if idx in bnd_connectivity.ravel():
                    if points_labels is None:
                        label = 1
                    else:
                        label = points_labels[idx]
                else:
                    label = 0
                fid.write('%.9f %.9f %d\n'%(points[idx,0],points[idx,1],label))
            for idx in range(connectivity.shape[0]):
                fid.write('%d %d %d %d\n'%(connectivity[idx,0]+1,connectivity[idx,1]+1,connectivity[idx,2]+1,0))
            if points_labels is None:
                bnd_labels = np.ones(bnd_connectivity.shape[0])
            else:
                bnd_labels = points_labels[bnd_connectivity[:,-1]]
            order = bnd_labels.argsort()
            bnd_labels = bnd_labels[order]
            bnd_connectivity = bnd_connectivity[order, :]
            for idx in range(bnd_connectivity.shape[0]):
                fid.write('%d %d %d\n'%(bnd_connectivity[idx,0]+1,bnd_connectivity[idx,1]+1,bnd_labels[idx]))
        #
        return prefix, prefix+ffemname, '.dat'

    def write_ffem_velocities(self, prefix='BASE/ffem-', suffix='.dat'):
        #
        instant = self.base[0][0]
        #
        if self.axi:
            if ('ux', 'node') not in instant.keys():
                self.base.compute('ux = u')
            if ('ut', 'node') not in instant.keys():
                self.base.compute('ut = utheta')
            velocities = [instant['ur'], instant['ut'], instant['ux']]
            #
            fids = [None]*3
            fids[0] = open('%sur%s'%(prefix, suffix), 'w')
            fids[1] = open('%sut%s'%(prefix, suffix), 'w')
            fids[2] = open('%sux%s'%(prefix, suffix), 'w')
            #
            for fid in fids:
                fid.write('%d\n\t'%velocities[0].size)
            #
            counter = 1
            for idx in range(velocities[0].size):
                for jdx,fid in enumerate(fids):
                    fid.write('%.9e'%(velocities[jdx][idx]))
                if counter < 5:
                    for fid in fids:
                        fid.write(' ')
                    counter += 1
                else:
                    for fid in fids:
                        fid.write('\n\t')
                    counter = 1
            #
            for fid in fids:
                fid.close()
            #
            return '%sur%s'%(prefix, suffix), '%sut%s'%(prefix, suffix), '%sux%s'%(prefix, suffix), suffix
        elif self.is2d:
            if np.any([vel not in instant.keys(location='node') for vel in ['ux', 'uy']]):
                print( 'Fatal error: ''2d'' planned but cannot find all ''ux'' and ''uy'' in baseflow.')
                exit()
            velocities = [instant['ux'], instant['uy']]
            #
            fids = [None]*2
            fids[0] = open('%sux%s'%(prefix, suffix), 'w')
            fids[1] = open('%suy%s'%(prefix, suffix), 'w')
            #
            for fid in fids:
                fid.write('%d\n\t'%velocities[0].size)
            #
            counter = 1
            for idx in range(velocities[0].size):
                for jdx,fid in enumerate(fids):
                    fid.write('%.9e'%(velocities[jdx][idx]))
                if counter < 5:
                    for fid in fids:
                        fid.write(' ')
                    counter += 1
                else:
                    for fid in fids:
                        fid.write('\n\t')
                    counter = 1
            #
            for fid in fids:
                fid.close()
            #
            return '%sux%s'%(prefix, suffix), '%suy%s'%(prefix, suffix), suffix
        elif self.is3d:
            if np.any([vel not in instant.keys(location='node') for vel in ['ux', 'uy', 'uz']]):
                print( 'Fatal error: ''3d'' planned but cannot find all ''ux'', ''uy'' and ''uz'' in baseflow.')
                exit()
            velocities = [instant['ux'], instant['uy'], instant['uz']]
            #
            fids = [None]*3
            fids[0] = open('%sux%s'%(prefix, suffix), 'w')
            fids[1] = open('%suy%s'%(prefix, suffix), 'w')
            fids[2] = open('%suz%s'%(prefix, suffix), 'w')
            #
            for fid in fids:
                fid.write('%d\n\t'%velocities[0].size)
            #
            counter = 1
            for idx in range(velocities[0].size):
                for jdx,fid in enumerate(fids):
                    fid.write('%.9e'%(velocities[jdx][idx]))
                if counter < 5:
                    for fid in fids:
                        fid.write(' ')
                    counter += 1
                else:
                    for fid in fids:
                        fid.write('\n\t')
                    counter = 1
            #
            for fid in fids:
                fid.close()
            #
            return '%sux%s'%(prefix, suffix), '%suy%s'%(prefix, suffix), '%suz%s'%(prefix, suffix), suffix    

    def write_ffem_viscosity(self, c_nu, visco_turb=False, visco_law=None, mu_ref=None, T_ref=None, coeff=None, prefix='BASE/ffem-', suffix='.dat'):
        #
        instant = self.base[0][0]
        #
        if visco_law is None or visco_law == 'constant':
            instant['nu'] = c_nu*np.ones(instant.shape)
            densitynames = ['rho', 'ro', 'density', 'Density']
            density_exist = [densityname in instant.keys(location='node') for densityname in densitynames]
            if np.any(density_exist):
                densityname = densitynames[np.argmax(density_exist)]
                instant['mu'] = instant['nu'] * instant[densityname]
        else:
            tempnames = ['T', 'temp', 'Ts', 'Temperature', 'temperature', 'tsta']
            temp_exist = [tempname in instant.keys(location='node') for tempname in tempnames]
            densitynames = ['rho', 'ro', 'density', 'Density']
            density_exist = [densityname in instant.keys(location='node') for densityname in densitynames]            
            if visco_law == 'sutherland':
                if not np.any(temp_exist) or not np.any(density_exist):
                    print( 'Fatal error: ''visco_law'' set but no temperature or density field in baseflow')
                    exit()
                tempname = tempnames[np.argmax(temp_exist)]
                densityname = densitynames[np.argmax(density_exist)]
                instant['mu'] = mu_ref * (instant[tempname] / T_ref)**(3./2.) * ( T_ref + coeff )/(instant[tempname] + coeff )
                instant['nu'] = instant['mu']/instant[densityname]
            elif visco_law == 'power':
                if not np.any(temp_exist) or not np.any(density_exist):
                    print( 'Fatal error: ''visco_law'' set but no temperature or density field in baseflow')
                    exit()
                tempname = tempnames[np.argmax(temp_exist)]
                densityname = densitynames[np.argmax(density_exist)]
                instant['mu'] = mu_ref * (instant[tempname] / T_ref) ** coeff
                instant['nu'] = instant['mu'] / instant[densityname]
            else:
                print( 'Other ''visco_law'' than ''sutherland'' and ''power'' not implemented')
                exit()
        #
        if visco_turb:
            visturbnames = ['vis_turb', 'visco_turb', 'nuT', 'nut']
            visturb_exist = [visturbname in instant.keys(location='node') for visturbname in visturbnames]
            if np.any(visturb_exist):
                visturbname = visturbnames[np.argmax(visturb_exist)]
                instant['nu'] = instant['nu'] + instant[visturbname]
                #
                densitynames = ['rho', 'ro', 'density', 'Density']
                density_exist = [densityname in instant.keys(location='node') for densityname in densitynames]
                if np.any(density_exist):
                    densityname = densitynames[np.argmax(density_exist)]
                    instant['mu'] = instant['mu'] + instant[visturbname] * instant[densityname]
        #
        if 'mu' in instant.keys(location='node'):
            fid = open('%smu%s'%(prefix, suffix), 'w')
            fid.write('%d\n\t'%instant['mu'].size)
            counter = 1
            for idx in range(instant['mu'].size):
                fid.write('%.9e'%(instant['mu'][idx]))
                if counter < 5:
                    fid.write(' ')
                    counter += 1
                else:
                    fid.write('\n\t')
                    counter = 1
            fid.close()
        #
        fid = open('%snu%s'%(prefix, suffix), 'w')
        fid.write('%d\n\t'%instant['nu'].size)
        counter = 1
        for idx in range(instant['nu'].size):
            fid.write('%.9e'%(instant['nu'][idx]))
            if counter < 5:
                fid.write(' ')
                counter += 1
            else:
                fid.write('\n\t')
                counter = 1
        fid.close()
        #
        return '%smu%s'%(prefix, suffix), '%snu%s'%(prefix, suffix)

    def write_ffem_compvars(self, gamma, cp, pr, visco_law, T_ref, mu_ref, coeff, prefix='BASE/ffem-', suffix='.dat'):
        #
        if not self.isComp:
            return
        #
        instant = self.base[0][0]
        #
        tempnames = ['T', 'temp', 'Ts', 'Temperature', 'temperature', 'tsta']
        temp_exist = [tempname in instant.keys(location='node') for tempname in tempnames]
        densitynames = ['rho', 'ro', 'density', 'Density']
        density_exist = [densityname in instant.keys(location='node') for densityname in densitynames]
        pressurenames = ['press', 'p', 'Ps', 'pressure', 'Pressure']
        pressure_exist = [pressurename in instant.keys(location='node') for pressurename in pressurenames]
        #
        if not np.any(temp_exist) or not np.any(density_exist):
            print( 'Fatal error: compressible computation set but no temperature or density field in baseflow')
            exit()
        #
        tempname = tempnames[np.argmax(temp_exist)]
        densityname = densitynames[np.argmax(density_exist)]
        #        
        if not np.any(pressure_exist):
            instant[pressurenames[0]] = instant[tempname] * instant[densityname] *cp*(gamma-1.0)/gamma
            pressurename = pressurenames[0]
        else:
            pressurename = pressurenames[np.argmax(pressure_exist)]
        #
        vars = [instant[densityname], instant[tempname], instant[pressurename]]
        fids = [None]*3
        fids[0] = open('%srho%s'%(prefix, suffix), 'w')
        fids[1] = open('%stemp%s'%(prefix, suffix), 'w')
        fids[2] = open('%spress%s'%(prefix, suffix), 'w')
        for fid in fids:
            fid.write('%d\n\t'%vars[0].size)
        counter = 1
        for idx in range(vars[0].size):
            for jdx,fid in enumerate(fids):
                fid.write('%.9e'%(vars[jdx][idx]))
            if counter < 5:
                for fid in fids:
                    fid.write(' ')
                counter += 1
            else:
                for fid in fids:
                    fid.write('\n\t')
                counter = 1
        for fid in fids:
            fid.close()
        #
        instant['gamma'] = gamma*np.ones(instant.shape)
        instant['Cp'] = cp*np.ones(instant.shape)
        instant['kappa'] = instant['mu'] * instant['Cp'] / pr
        #
        vars = [instant['gamma'], instant['Cp'], instant['kappa']]
        fids = [None]*3
        fids[0] = open('%sgamma%s'%(prefix, suffix), 'w')
        fids[1] = open('%scp%s'%(prefix, suffix), 'w')
        fids[2] = open('%skappa%s'%(prefix, suffix), 'w')
        for fid in fids:
            fid.write('%d\n\t'%vars[0].size)
        counter = 1
        for idx in range(vars[0].size):
            for jdx,fid in enumerate(fids):
                fid.write('%.9e'%(vars[jdx][idx]))
            if counter < 5:
                for fid in fids:
                    fid.write(' ')
                counter += 1
            else:
                for fid in fids:
                    fid.write('\n\t')
                counter = 1
        for fid in fids:
            fid.close()
        #
        if visco_law is None or visco_law == 'constant':
            instant['dmudT'] = np.zeros(instant.shape)
            instant['d2mudT2'] = np.zeros(instant.shape)
        elif visco_law == 'sutherland':
            # mu_ref * (instant[tempname] / T_ref)**(3./2.) * ( T_ref + coeff )/(instant[tempname] + coeff )
            instant['dmudT'] = mu_ref*(T_ref+coeff)*(3*coeff+instant[tempname])*np.sqrt(instant[tempname]/T_ref)/(2.0*T_ref*(instant[tempname]+coeff)**2)
            instant['d2mudT2'] = -mu_ref*(T_ref+coeff)*(-3.0*coeff**2+6.0*coeff*instant[tempname]+instant[tempname]**2)/(4.0*T_ref**2*(coeff+instant[tempname])**3*np.sqrt(instant[tempname]/T_ref))
        elif visco_law == 'power':
            # instant['mu'] = mu_ref * (instant[tempname] / T_ref) ** coeff
            instant['dmudT'] = mu_ref * (1.0 / T_ref)**coeff * coeff*instant[tempname]**(coeff-1)
            instant['d2mudT2'] = mu_ref * (1.0 / T_ref)**coeff * coeff*(coeff-1.)*instant[tempname]**(coeff-2)
        instant['dkappadT'] = instant['Cp'] * instant['dmudT'] / pr
        instant['d2kappadT2'] = instant['Cp'] * instant['d2mudT2'] / pr
        #
        vars = [instant['dmudT'], instant['d2mudT2'], instant['dkappadT'], instant['d2kappadT2']]
        fids = [None]*4
        fids[0] = open('%sdmudT%s'%(prefix, suffix), 'w')
        fids[1] = open('%sd2mudT2%s'%(prefix, suffix), 'w')
        fids[2] = open('%sdkappadT%s'%(prefix, suffix), 'w')
        fids[3] = open('%sd2kappadT2%s'%(prefix, suffix), 'w')
        for fid in fids:
            fid.write('%d\n\t'%vars[0].size)
        counter = 1
        for idx in range(vars[0].size):
            for jdx,fid in enumerate(fids):
                fid.write('%.9e'%(vars[jdx][idx]))
            if counter < 5:
                for fid in fids:
                    fid.write(' ')
                counter += 1
            else:
                for fid in fids:
                    fid.write('\n\t')
                counter = 1
        for fid in fids:
            fid.close()
        #
        return '%srho%s'%(prefix, suffix), '%stemp%s'%(prefix, suffix), '%spress%s'%(prefix, suffix), \
               '%sgamma%s'%(prefix, suffix), '%scp%s'%(prefix, suffix), '%skappa%s'%(prefix, suffix), \
               '%sdmudT%s'%(prefix, suffix), '%sd2mudT2%s'%(prefix, suffix), \
               '%sdkappadT%s'%(prefix, suffix), '%sd2kappadT2%s'%(prefix, suffix), suffix    

    def _find_boundaries(self, ubounds=None, prefix='BASE/ffem-', suffix='.dat'):
        #
        # create source points
        points = np.empty((self.base[0][0].shape[0],2))
        points[:,0] = self.base[0][0]['x']
        points[:,1] = self.base[0][0]['y']
        # store source connectivity
        connectivity = self.base[0][0].connectivity['tri']
        #
        # extract the boundary points (skin)
        tr = antares.Treatment('extractbounds')
        tr['base'] = self.base
        result = tr.execute()
        r_points = np.zeros((result[0][0].shape[0],2))
        r_points[:,0] = result[0][0]['x']
        r_points[:,1] = result[0][0]['y']
        #
        # match the boundary points index to the old connectivity
        old_connectivity = []
        for idx in range(r_points.shape[0]):
            mask = np.where(points[:,0] == r_points[idx,0])[0]      # for x
            possible_y = points[:,1][mask]
            index_y = np.where(possible_y == r_points[idx,1])[0][0] # for y
            old_connectivity.append(mask[index_y])
        #
        bnd_connectivity = copy(result[0][0].connectivity['bi'])
        for idx in range(bnd_connectivity.shape[0]):
            strut = bnd_connectivity[idx,:]
            for jdx in range(strut.size):
                strut[jdx] = old_connectivity[strut[jdx]]
        #
        if ubounds is not None:
            # assign label to boundary nodes
            points_labels = np.ones(points.shape[0])
            bnd_pts_idx = np.unique(bnd_connectivity.ravel())
            for idx, key in enumerate(ubounds.keys()):
                if ubounds[key]['x'] is not None:
                    if type(ubounds[key]['x']) is str:
                        if ubounds[key]['x'] == 'xmin':
                            mask = np.isclose(points[bnd_pts_idx, 0], points[:,0].min())
                        elif ubounds[key]['x'] == 'xmax':
                            mask = np.isclose(points[bnd_pts_idx, 0], points[:,0].max())
                    else:
                        mask = np.isclose(points[bnd_pts_idx, 0], ubounds[key]['x'])
                elif ubounds[key]['y'] is not None:
                    if type(ubounds[key]['y']) is str:
                        if ubounds[key]['y'] == 'ymin':
                            mask = np.isclose(points[bnd_pts_idx, 1], points[:,1].min())
                        elif ubounds[key]['y'] == 'ymax':
                            mask = np.isclose(points[bnd_pts_idx, 1], points[:,1].max())
                    else:
                        mask = np.isclose(points[bnd_pts_idx, 1], ubounds[key]['y'])
                elif ubounds[key]['z'] is not None:
                    mask = np.isclose(points[bnd_pts_idx, 2], ubounds[key]['z'])
                else:
                    if ubounds[key]['label'] == 1:
                        mask = np.zeros(bnd_pts_idx.shape, dtype=bool)
                    else:
                        print( ' Fatal error: no coordinate mask for boundary condition %s.'%key)
                        exit()
                points_labels[bnd_pts_idx[mask]] = ubounds[key]['label']
            # dump the ffem formatted bocos
            if self.axi:
                if self.isComp:
                    variables = ['rho', 'ur', 'ut', 'uz', 'temp']
                else:
                    variables = ['ur', 'ut', 'uz', 'p']
            elif self.is2d:
                variables = ['ux', 'uy', 'p']
            elif self.is3d:
                if self.isComp:
                    variables = ['rho', 'ux', 'uy', 'uz', 'temp'] 
                else:
                    variables = ['ux', 'uy', 'uz', 'p']
            for idx, key in enumerate(ubounds.keys()):
                c_label = ubounds[key]['label']
                labelfile = prefix+'boco_'+str(c_label)+suffix
                with open(labelfile, 'w') as fid:
                    fid.write('%d\n'%(len(variables)))
                    for var in variables:
                        number = 1 if ubounds[key][var]=='dirichlet' else 0
                        fid.write('\t%d\n'%number)
        #
        return points, connectivity, bnd_connectivity, points_labels if ubounds is not None else None

