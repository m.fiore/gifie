from antares import *
import os
import numpy as np

import matplotlib as mpl
font={'family':'serif','weight':'medium','size':30}
mpl.rc('font',**font)
mpl.rcParams['axes.linewidth'] = 3
mpl.rc('text', usetex=True)
import matplotlib.pyplot as plt
import matplotlib.cm as cm

# ---------------------------------------

dir = 'MODES_MELIGA'
print dir

filename_reduced = os.path.join(dir, 'mode1_dir_reduced.plt')
if os.path.isfile(filename_reduced):
	reader = Reader('bin_tp')
	reader['filename'] = filename_reduced
	base = reader.read()

else:
	filename = os.path.join(dir, 'mode1_dir.plt')

	reader = Reader('bin_tp')
	reader['filename'] = filename
	base = reader.read()
	print base[0][0]
	base = base[:,:,('X', 'Y', 'wr')]

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [14.,0.,0.]
	tr['normal'] = [1.,0.,0.]
	base = tr.execute()

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [-14.,0.,0.]
	tr['normal'] = [-1.,0.,0.]
	base = tr.execute()

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [0.,4.,0.]
	tr['normal'] = [0.,1.,0.]
	base = tr.execute()

	writer = Writer('bin_tp')
	writer['base'] = base
	writer['filename'] = os.path.join(dir,'mode1_dir_reduced.plt')
	writer.dump()

# print base[0][0]['uz_00001_re'].min(), base[0][0]['uz_00001_re'].max()
# print base[0][0]['uz_00001_im'].min(), base[0][0]['uz_00001_im'].max()

tr = Treatment('extractbounds')
tr['base'] = base
bounds = tr.execute()

toplot = base[0][0]['wr']
toplotmax = abs(toplot).max()
toplot /= toplotmax
# toplot -= 1.0
# toplot *= 1.4/2.0

print toplot.min(), toplot.max()

fig = plt.figure(1, figsize=(28,4), dpi=100)
ax = plt.subplot(111)
sphere = ax.plot(0.5*np.cos(np.linspace(0.0,np.pi,20)), 0.5*np.sin(np.linspace(0.0,np.pi,20)), '-k', lw=3)
im = ax.tricontourf(base[0][0]['X'], base[0][0]['Y'], base[0][0].connectivity['tri'], toplot, 
					np.linspace(-1.0, 1.0, 201), cmap=cm.cubehelix, vmin=-1.0, vmax=1.0, extend='both')
					# np.linspace(-1.4, 0.02, 201), cmap=cm.cubehelix, vmin=-1.4, vmax=0.02, extend='both')
					# np.linspace(-1.4, -0.65, 101), cmap=cm.seismic, vmin=-1.4, vmax=-0.65, extend='both')
cb = plt.colorbar(im, ax=ax, pad=0.01)
cb.set_ticks([-1.0,1.0]) #[-1.4, 0.02])
cb.set_ticklabels(['-1','1']) #['-1.4', '0.02'])
ax.set_xlim(-14, 14)
ax.set_xticks([-12, -8, -4, 0, 4, 8 ,12])
ax.set_xticklabels(['-12', '-8', '-4', '0', '4', '8', '12'])
ax.set_ylim(0, 4.5)
ax.set_yticks([0, 2, 4])
ax.set_yticklabels(['0', '2', '4'])
ax.axis('equal')
ax.axis('tight')
ax.tick_params(which='major',width=2,length=15,direction='out')
plt.savefig(dir+'/stationary_mode.png', bbox_inches='tight')
plt.close()

# ---------------------------------------

dir = 'MODES_MELIGA'
print dir

filename_reduced = os.path.join(dir, 'mode2_dir_reduced.plt')
if os.path.isfile(filename_reduced):
	reader = Reader('bin_tp')
	reader['filename'] = filename_reduced
	base = reader.read()

else:
	filename = os.path.join(dir, 'mode2_dir.plt')

	reader = Reader('bin_tp')
	reader['filename'] = filename
	base = reader.read()
	print base
	base = base[:,:,('X', 'Y', 'wr')]

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [14.,0.,0.]
	tr['normal'] = [1.,0.,0.]
	base = tr.execute()

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [-14.,0.,0.]
	tr['normal'] = [-1.,0.,0.]
	base = tr.execute()

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [0.,4.,0.]
	tr['normal'] = [0.,1.,0.]
	base = tr.execute()

	writer = Writer('bin_tp')
	writer['base'] = base
	writer['filename'] = os.path.join(dir,'mode2_dir_reduced.plt')
	writer.dump()

# print base[0][0]['uz_00001_re'].min(), base[0][0]['uz_00001_re'].max()
# print base[0][0]['uz_00001_im'].min(), base[0][0]['uz_00001_im'].max()

tr = Treatment('extractbounds')
tr['base'] = base
bounds = tr.execute()

toplot = base[0][0]['wr']
toplotmax = abs(toplot).max()
toplot /= toplotmax
# toplot -= 1.0
# toplot *= 1.4/2.0

print toplot.min(), toplot.max()

fig = plt.figure(1, figsize=(28,4), dpi=100)
ax = plt.subplot(111)
sphere = ax.plot(0.5*np.cos(np.linspace(0.0,np.pi,20)), 0.5*np.sin(np.linspace(0.0,np.pi,20)), '-k', lw=3)
im = ax.tricontourf(base[0][0]['X'], base[0][0]['Y'], base[0][0].connectivity['tri'], toplot, 
					np.linspace(-1, 1, 201), cmap=cm.cubehelix, vmin=-1, vmax=1, extend='both')
					# np.linspace(-1.4, 1.4, 201), cmap=cm.cubehelix, vmin=-1.4, vmax=1.4, extend='both')
cb = plt.colorbar(im, ax=ax, pad=0.01)
cb.set_ticks([-1, 1])
cb.set_ticklabels(['-1', '1'])
ax.set_xlim(-14, 14)
ax.set_xticks([-12, -8, -4, 0, 4, 8 ,12])
ax.set_xticklabels(['-12', '-8', '-4', '0', '4', '8', '12'])
ax.set_ylim(0, 4.5)
ax.set_yticks([0, 2, 4])
ax.set_yticklabels(['0', '2', '4'])
ax.axis('equal')
ax.axis('tight')
ax.tick_params(which='major',width=2,length=15,direction='out')
plt.savefig(dir+'/oscillatory_mode.png', bbox_inches='tight')
plt.close()

exit()

# ---------------------------------------

dir = 'MODES_MELIGA'
print dir

filename_reduced = os.path.join(dir, 'base_p2_reduced.plt')
if os.path.isfile(filename_reduced):
	reader = Reader('bin_tp')
	reader['filename'] = filename_reduced
	base = reader.read()

else:
	filename = os.path.join(dir, 'base_p2.plt')

	reader = Reader('bin_tp')
	reader['filename'] = filename
	base = reader.read()

	base = base[:,:,('x', 'y', 'z', 'uz_00001_re', 'uz_00001_im')]

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [4,0,0] #[14.,0.,0.]
	tr['normal'] = [1.,0.,0.]
	base = tr.execute()

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [-4,0,0] #[-14.,0.,0.]
	tr['normal'] = [-1.,0.,0.]
	base = tr.execute()

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [0,2,0] #[0.,4.,0.]
	tr['normal'] = [0.,1.,0.]
	base = tr.execute()

	writer = Writer('bin_tp')
	writer['base'] = base
	writer['filename'] = 'POSTPROCESS/M1_RE280_ADJOINT_REFINED/base_p2_reduced.plt'
	writer.dump()

print base[0][0]['uz_00001_re'].min(), base[0][0]['uz_00001_re'].max()
print base[0][0]['uz_00001_im'].min(), base[0][0]['uz_00001_im'].max()

tr = Treatment('extractbounds')
tr['base'] = base
bounds = tr.execute()

fig = plt.figure(1, figsize=(16,4), dpi=300)
ax = plt.subplot(111)
sphere = ax.plot(0.5*np.cos(np.linspace(0.0,np.pi,20)), 0.5*np.sin(np.linspace(0.0,np.pi,20)), '-k', lw=3)
im = ax.tricontourf(base[0][0]['x'], base[0][0]['y'], base[0][0].connectivity['tri'], base[0][0]['uz_00001_re'], 
					np.linspace(-1.6e-13, 4.0e-14, 201), cmap=cm.cubehelix)#, vmin=-2.5e-14, vmax=2.5e-14, extend='both')
					# 201, cmap=cm.jet, vmin=-1.4e-03, vmax=1.4e-03, extend='both')
cb = plt.colorbar(im, ax=ax, pad=0.01)
cb.set_ticks([-1.6e-13, 4.0e-14])
cb.set_ticklabels(['-16', '4'])
# levels = np.linspace(-1.5e-14, 2.5e-14, 5)
levels = np.array([-25,-15,-10,-7,-6,-5,5,6,7,10,15,25]) * 1.0e-15
# levels = [-1.5e-14,-1.0e-14,-7e-15,-6e-15,-5.e-15,5.e-15,1.5e-14,2.5e-14]
print levels
cs = ax.tricontour(base[0][0]['x'], base[0][0]['y'], base[0][0].connectivity['tri'], base[0][0]['uz_00001_re'], 
				   levels, colors='k', linewidths=1.5)
# plt.clabel(cs, levels, inline=True, fmt=r'%.1e')
ax.set_xlim(-4,4) #(-14, 14)
ax.set_xticks([-4, -2, 0, 2, 4]) #([-12, -8, -4, 0, 4, 8 ,12])
ax.set_xticklabels(['-4', '-2', '0', '2', '4']) #(['-12', '-8', '-4', '0', '4', '8', '12'])
ax.set_ylim(0,2) #(0, 4.5)
ax.set_yticks([0,1,2]) #([0, 2, 4])
ax.set_yticklabels(['0', '1', '2']) #(['0', '2', '4'])
ax.set_xlabel(r'z')
ax.set_ylabel(r'r')
ax.axis('equal')
# ax.axis('tight')
ax.set_adjustable('box-forced')
ax.tick_params(which='major',width=2,length=15,direction='out')
plt.savefig(dir+'/oscillatory_mode.png', bbox_inches='tight')
plt.close()
