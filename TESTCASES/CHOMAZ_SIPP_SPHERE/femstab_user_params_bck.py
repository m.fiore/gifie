# ---------------------------------------
# All parameters to a biglobal analysis
# ---------------------------------------

# baseflow file location
basename = 'BASE/sphere_flow.plt'

# location of the stability matrices
linloc = 'LIN'

# location of outputs and post-processing
pploc = 'POSTPROCESS'

# ---------------------------------------

boundaries = {'top wall':{'x':None,'y':'ymax','z':None,
                        'ur':'neumann',
                        'ut':'neumann',
                        'uz':'neumann',
                        'p':'neumann',
                        'label':4},
              'axis':{'x':None, 'y':'ymin','z':None,
                       'ur':'neumann',
                       'ut':'neumann',
                       'uz':'dirichlet',
                       'p':'dirichlet',
                       'label':2},
              'sphere':{'x':None, 'y':None, 'z':None,
                        'ur':'dirichlet',
                        'ut':'dirichlet',
                        'uz':'dirichlet',
                        'p':'neumann',
                        'label':1},
              'inlet':{'x':'xmin','y':None,'z':None,
                      'ur':'dirichlet',
                      'ut':'dirichlet',
                      'uz':'dirichlet',
                      'p':'neumann',
                      'label':5},
              'outlet':{'x':'xmax','y':None,'z':None,
                        'ur':'neumann',
                        'ut':'neumann',
                        'uz':'neumann',
                        'p':'neumann',
                        'label':3}}

# ---------------------------------------

# average kinematic viscosity
# set visco_law to 'sutherland' to consider a field of visco
visco_law = None
mu_ref = 0.004703668862 # re = 212.6
# mu_ref = 0.003562522266 # re = 280.7
T_ref = 300.0
coeff = 110.6
rho_ref = 1.0
visco_turb = False

# For compressible flows only now
gamma = 1.4
Cp = 1005.2
Pr = 0.71

# linearized equations to be solved, among:
## axi_incompressible_NS
## incompressible_NS_2d
## incompressible_NS_3d
## compressible_NS_3d
equations = 'axi_incompressible_NS'

# ---------------------------------------

# azimuthal wavenumber
## axi_incompressible_NS : True
## compressible_Euler_Poisson : False
## incompressible_NS_2d : False
## incompressible_NS_3d : False
axisymmetric = True
mmin = 1
mmax = 1
mstep = 1

# depth wavenumber in the Z direction in case of 3D
## axi_incompressible_NS : False
## compressible_Euler_Poisson : False
## incompressible_NS_2d : False
## incompressible_NS_3d : True
is3d = False
kmin = 0
kmax = 10
kstep = 1

# ---------------------------------------

# mesh refinement
refinement = False
ratio = 1.1
hmax = 0.06

# number of proc for PETSc/SLEPc solution
mpiprocs = 15

# targets for PETSc/SLEPc solution process
# WARNING :: omega in RAD/S
om_min = 0.0 #0.7
om_max = 0.0 #0.7
om_num = 1
nev = 20
ncv = 60

# ---------------------------------------

verbosity = 1
