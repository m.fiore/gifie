from antares import *
import os
import numpy as np

import matplotlib as mpl
font={'family':'serif','weight':'medium','size':30}
mpl.rc('font',**font)
mpl.rcParams['axes.linewidth'] = 3
mpl.rc('text', usetex=True)
import matplotlib.pyplot as plt
import matplotlib.cm as cm

# ---------------------------------------
"""
dir = 'BASE'
print dir

filename = os.path.join(dir, 'sphere_flow_re212_meliga_mesh.plt')

reader = Reader('bin_tp')
reader['filename'] = filename
base = reader.read()

tr = Treatment('clip')
tr['base'] = base
tr['type'] = 'plane'
tr['origin'] = [5.,0.,0.]
tr['normal'] = [1.,0.,0.]
base = tr.execute()

tr = Treatment('clip')
tr['base'] = base
tr['type'] = 'plane'
tr['origin'] = [-1.5,0.,0.]
tr['normal'] = [-1.,0.,0.]
base = tr.execute()

tr = Treatment('clip')
tr['base'] = base
tr['type'] = 'plane'
tr['origin'] = [0.,2.,0.]
tr['normal'] = [0.,1.,0.]
base = tr.execute()

print base[0][0]['ux'].min(), base[0][0]['ux'].max()

tr = Treatment('extractbounds')
tr['base'] = base
bounds = tr.execute()

fig = plt.figure(1, figsize=(10.5625,3.25), dpi=300)
ax = plt.subplot(111)
sphere = ax.plot(0.5*np.cos(np.linspace(0.0,np.pi,20)), 0.5*np.sin(np.linspace(0.0,np.pi,20)), '-k', lw=3)
im = ax.tricontourf(base[0][0]['x'], base[0][0]['y'], base[0][0].connectivity['tri'], base[0][0]['ux'], 
					np.linspace(-0.4, 1.1, 101), cmap=cm.seismic, vmin=-0.4, vmax=1.1, extend='both')
cb = plt.colorbar(im, ax=ax, pad=0.01)
cb.set_ticks([-0.4, 1.1])
cb.set_ticklabels(['-0.4', '1.1'])
levels = [-0.2, 0, 0.2, 0.4, 0.6, 0.8]
cs = ax.tricontour(base[0][0]['x'], base[0][0]['y'], base[0][0].connectivity['tri'], base[0][0]['ux'], 
			  levels, colors='k', linewidths=1.5)
manual_locations = [(1.0, 0.1), (1.8, 0.2), (2.1, 0.2), (3.0, 0.3), (3.5, 0.4), (4.0, 0.5)]
plt.clabel(cs, levels, inline=True, inline_spacing=50, fmt=r'%.2f', fontsize=15, manual=manual_locations)
ax.set_xlim(-1.5, 5)
ax.set_xticks([0, 2, 4])
ax.set_xticklabels(['0', '2', '4'])
ax.set_xlabel(r'z')
ax.set_ylim(0, 2.0)
ax.set_yticks([0, 1, 2])
ax.set_yticklabels(['0', '1', '2'])
ax.set_ylabel(r'r')
ax.axis('equal')
ax .set_adjustable('box-forced')
ax.tick_params(which='major',width=2,length=15,direction='out')
plt.savefig(dir+'/sphere_flow_meliga_mesh.png', bbox_inches='tight')
plt.close()
"""
# ---------------------------------------

dir = 'POSTPROCESS/M1'
print dir

filename_reduced = os.path.join(dir, 'base_p2_reduced.plt')
if os.path.isfile(filename_reduced):
	reader = Reader('bin_tp')
	reader['filename'] = filename_reduced
	base = reader.read()

else:
	filename = os.path.join(dir, 'base_p2.plt')

	reader = Reader('bin_tp')
	reader['filename'] = filename
	base = reader.read()

	nmode = 0
	for key in base[0][0].keys(location='node'):
		if not key in ['x','y','z']:
			if 'uz' in key and 're' in key:
				nmode += 1

	for mode in range(1, nmode+1):
		base[0][0]['ur_%05d_mod'%mode] = np.absolute( base[0][0]['ur_%05d_re'%mode] + 1j*base[0][0]['ur_%05d_im'%mode] )
		base[0][0]['ur_%05d_phi'%mode] = np.arctan2( base[0][0]['ur_%05d_im'%mode], base[0][0]['ur_%05d_re'%mode] )
		base[0][0]['ut_%05d_mod'%mode] = np.absolute( base[0][0]['ut_%05d_re'%mode] + 1j*base[0][0]['ut_%05d_im'%mode] )
		base[0][0]['ut_%05d_phi'%mode] = np.arctan2( base[0][0]['ut_%05d_im'%mode], base[0][0]['ut_%05d_re'%mode] )
		base[0][0]['uz_%05d_mod'%mode] = np.absolute( base[0][0]['uz_%05d_re'%mode] + 1j*base[0][0]['uz_%05d_im'%mode] )
		base[0][0]['uz_%05d_phi'%mode] = np.arctan2( base[0][0]['uz_%05d_im'%mode], base[0][0]['uz_%05d_re'%mode] )

	X = base[0][0]['x']
	Y = base[0][0]['y']
	target = [0,1]
	points = np.vstack((X,Y)).T
	closest = np.argmin(np.sqrt(np.sum((points-target)*(points-target), axis=1)))

	for mode in range(1, nmode+1):
		phase_at_closest = base[0][0]['ur_%05d_phi'%mode][closest]
		base[0][0]['ur_%05d_phi'%mode] = base[0][0]['ur_%05d_phi'%mode] - phase_at_closest

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [5.25,0.,0.]
	tr['normal'] = [1.,0.,0.]
	res = tr.execute()

	tr = Treatment('clip')
	tr['base'] = res
	tr['type'] = 'plane'
	tr['origin'] = [-2.5,0.,0.]
	tr['normal'] = [-1.,0.,0.]
	res = tr.execute()

	tr = Treatment('clip')
	tr['base'] = res
	tr['type'] = 'plane'
	tr['origin'] = [0.,2.,0.]
	tr['normal'] = [0.,1.,0.]
	res = tr.execute()

	energies = np.zeros(nmode)
	for mode in range(1, nmode+1):
		idx = mode - 1
		urc = res[0][0]['ur_%05d_re'%mode] + 1j*res[0][0]['ur_%05d_im'%mode]
		utc = res[0][0]['ut_%05d_re'%mode] + 1j*res[0][0]['ut_%05d_im'%mode]
		uzc = res[0][0]['uz_%05d_re'%mode] + 1j*res[0][0]['uz_%05d_im'%mode]
		energies[idx] = np.sum( np.absolute(urc)**2 + np.absolute(utc)**2 + np.absolute(uzc)**2 ) / urc.size

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [14.,0.,0.]
	tr['normal'] = [1.,0.,0.]
	res = tr.execute()

	tr = Treatment('clip')
	tr['base'] = res
	tr['type'] = 'plane'
	tr['origin'] = [-14.,0.,0.]
	tr['normal'] = [-1.,0.,0.]
	res = tr.execute()

	tr = Treatment('clip')
	tr['base'] = res
	tr['type'] = 'plane'
	tr['origin'] = [0.,4.,0.]
	tr['normal'] = [0.,1.,0.]
	res = tr.execute()

	# writer = Writer('bin_tp')
	# writer['base'] = base
	# writer['filename'] = os.path.join(dir,'base_p2_reduced.plt')
	# writer.dump()

for mode in range(1, nmode+1):

	# print res[0][0]['uz_%05d_re'%mode].min(), res[0][0]['uz_%05d_re'%mode].max()
	# print res[0][0]['uz_%05d_im'%mode].min(), res[0][0]['uz_%05d_im'%mode].max()
	print res[0][0]['uz_%05d_mod'%mode].min(), res[0][0]['uz_%05d_mod'%mode].max()

	tr = Treatment('extractbounds')
	tr['base'] = res
	bounds = tr.execute()

	# toplot = res[0][0]['uz_%05d_re'%mode]
	# toplotmax = abs(toplot).max()
	# toplot /= toplotmax
	# # toplot -= 1.0
	# # toplot *= 1.4/2.0
	toplot = res[0][0]['uz_%05d_mod'%mode]
	# toplotmax = toplot.max()
	# toplot /= toplotmax
	toplot /= np.sqrt(energies[mode-1])
	toplot = toplot * np.cos(res[0][0]['uz_%05d_phi'%mode])

	print toplot.min(), toplot.max()

	fig = plt.figure(1, figsize=(28,4), dpi=100)
	ax = plt.subplot(111)
	sphere = ax.plot(0.5*np.cos(np.linspace(0.0,np.pi,20)), 0.5*np.sin(np.linspace(0.0,np.pi,20)), '-k', lw=3)
	im = ax.tricontourf(res[0][0]['x'], res[0][0]['y'], res[0][0].connectivity['tri'], toplot, 
						np.linspace(-1.4, 1.4, 101), cmap=cm.cubehelix, extend='both')
	cb = plt.colorbar(im, ax=ax, pad=0.01)
	cb.set_ticks([-1.4, 1.4])
	cb.set_ticklabels(['-1.4', '1.4'])
	ax.set_xlim(-14, 14)
	ax.set_xticks([-12, -8, -4, 0, 4, 8 ,12])
	ax.set_xticklabels(['-12', '-8', '-4', '0', '4', '8', '12'])
	ax.set_ylim(0, 4.5)
	ax.set_yticks([0, 2, 4])
	ax.set_yticklabels(['0', '2', '4'])
	ax.axis('equal')
	ax.axis('tight')
	ax.tick_params(which='major',width=2,length=15,direction='out')
	# plt.savefig(dir+'/stationary_mode.png', bbox_inches='tight')
	plt.savefig(dir+'/uz_mode_%05d.png'%mode, bbox_inches='tight')
	plt.close()

exit()

# ---------------------------------------

dir = 'POSTPROCESS/M1_RE280'
print dir

filename_reduced = os.path.join(dir, 'base_p2_reduced.plt')
if os.path.isfile(filename_reduced):
	reader = Reader('bin_tp')
	reader['filename'] = filename_reduced
	base = reader.read()

else:
	filename = os.path.join(dir, 'base_p2.plt')

	reader = Reader('bin_tp')
	reader['filename'] = filename
	base = reader.read()

	base = base[:,:,('x', 'y', 'z', 'uz_00001_re', 'uz_00001_im')]

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [14.,0.,0.]
	tr['normal'] = [1.,0.,0.]
	base = tr.execute()

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [-14.,0.,0.]
	tr['normal'] = [-1.,0.,0.]
	base = tr.execute()

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [0.,4.,0.]
	tr['normal'] = [0.,1.,0.]
	base = tr.execute()

	writer = Writer('bin_tp')
	writer['base'] = base
	writer['filename'] = 'POSTPROCESS/M1_RE280/base_p2_reduced.plt'
	writer.dump()

print base[0][0]['uz_00001_re'].min(), base[0][0]['uz_00001_re'].max()
print base[0][0]['uz_00001_im'].min(), base[0][0]['uz_00001_im'].max()

tr = Treatment('extractbounds')
tr['base'] = base
bounds = tr.execute()

toplot = base[0][0]['uz_00001_im']
toplotmax = abs(toplot).max()
toplot /= toplotmax
# toplot -= 1.0
# toplot *= 1.4/2.0

print toplot.min(), toplot.max()

fig = plt.figure(1, figsize=(28,4), dpi=100)
ax = plt.subplot(111)
sphere = ax.plot(0.5*np.cos(np.linspace(0.0,np.pi,20)), 0.5*np.sin(np.linspace(0.0,np.pi,20)), '-k', lw=3)
im = ax.tricontourf(base[0][0]['x'], base[0][0]['y'], base[0][0].connectivity['tri'], toplot,
					np.linspace(-1, 1, 201), cmap=cm.cubehelix, vmin=-1, vmax=1, extend='both')
					# np.linspace(-1.4e-03, 1.4e-03, 201), cmap=cm.cubehelix, vmin=-1.4e-03, vmax=1.4e-03, extend='both')
cb = plt.colorbar(im, ax=ax, pad=0.01)
cb.set_ticks([-1,1]) #[-1.4e-03, 1.4e-03])
cb.set_ticklabels(['-1','1']) #['-1.4', '1.4'])
ax.set_xlim(-14, 14)
ax.set_xticks([-12, -8, -4, 0, 4, 8 ,12])
ax.set_xticklabels(['-12', '-8', '-4', '0', '4', '8', '12'])
ax.set_ylim(0, 4.5)
ax.set_yticks([0, 2, 4])
ax.set_yticklabels(['0', '2', '4'])
ax.axis('equal')
ax.axis('tight')
ax.tick_params(which='major',width=2,length=15,direction='out')
plt.savefig(dir+'/oscillatory_mode.png', bbox_inches='tight')
plt.close()

# ---------------------------------------
"""
dir = 'POSTPROCESS/M1_RE280_ADJOINT_REFINED'
print dir

filename_reduced = os.path.join(dir, 'base_p2_reduced.plt')
if os.path.isfile(filename_reduced):
	reader = Reader('bin_tp')
	reader['filename'] = filename_reduced
	base = reader.read()

else:
	filename = os.path.join(dir, 'base_p2.plt')

	reader = Reader('bin_tp')
	reader['filename'] = filename
	base = reader.read()

	base = base[:,:,('x', 'y', 'z', 'uz_00001_re', 'uz_00001_im')]

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [4,0,0] #[14.,0.,0.]
	tr['normal'] = [1.,0.,0.]
	base = tr.execute()

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [-4,0,0] #[-14.,0.,0.]
	tr['normal'] = [-1.,0.,0.]
	base = tr.execute()

	tr = Treatment('clip')
	tr['base'] = base
	tr['type'] = 'plane'
	tr['origin'] = [0,2,0] #[0.,4.,0.]
	tr['normal'] = [0.,1.,0.]
	base = tr.execute()

	writer = Writer('bin_tp')
	writer['base'] = base
	writer['filename'] = 'POSTPROCESS/M1_RE280_ADJOINT_REFINED/base_p2_reduced.plt'
	writer.dump()

print base[0][0]['uz_00001_re'].min(), base[0][0]['uz_00001_re'].max()
print base[0][0]['uz_00001_im'].min(), base[0][0]['uz_00001_im'].max()

tr = Treatment('extractbounds')
tr['base'] = base
bounds = tr.execute()

fig = plt.figure(1, figsize=(16,4), dpi=300)
ax = plt.subplot(111)
sphere = ax.plot(0.5*np.cos(np.linspace(0.0,np.pi,20)), 0.5*np.sin(np.linspace(0.0,np.pi,20)), '-k', lw=3)
im = ax.tricontourf(base[0][0]['x'], base[0][0]['y'], base[0][0].connectivity['tri'], base[0][0]['uz_00001_re'], 
					np.linspace(-1.6e-13, 4.0e-14, 201), cmap=cm.cubehelix)#, vmin=-2.5e-14, vmax=2.5e-14, extend='both')
					# 201, cmap=cm.jet, vmin=-1.4e-03, vmax=1.4e-03, extend='both')
cb = plt.colorbar(im, ax=ax, pad=0.01)
cb.set_ticks([-1.6e-13, 4.0e-14])
cb.set_ticklabels(['-16', '4'])
# levels = np.linspace(-1.5e-14, 2.5e-14, 5)
levels = np.array([-25,-15,-10,-7,-6,-5,5,6,7,10,15,25]) * 1.0e-15
# levels = [-1.5e-14,-1.0e-14,-7e-15,-6e-15,-5.e-15,5.e-15,1.5e-14,2.5e-14]
print levels
cs = ax.tricontour(base[0][0]['x'], base[0][0]['y'], base[0][0].connectivity['tri'], base[0][0]['uz_00001_re'], 
				   levels, colors='k', linewidths=1.5)
# plt.clabel(cs, levels, inline=True, fmt=r'%.1e')
ax.set_xlim(-4,4) #(-14, 14)
ax.set_xticks([-4, -2, 0, 2, 4]) #([-12, -8, -4, 0, 4, 8 ,12])
ax.set_xticklabels(['-4', '-2', '0', '2', '4']) #(['-12', '-8', '-4', '0', '4', '8', '12'])
ax.set_ylim(0,2) #(0, 4.5)
ax.set_yticks([0,1,2]) #([0, 2, 4])
ax.set_yticklabels(['0', '1', '2']) #(['0', '2', '4'])
ax.set_xlabel(r'z')
ax.set_ylabel(r'r')
ax.axis('equal')
# ax.axis('tight')
ax.set_adjustable('box-forced')
ax.tick_params(which='major',width=2,length=15,direction='out')
plt.savefig(dir+'/oscillatory_mode.png', bbox_inches='tight')
plt.close()
"""