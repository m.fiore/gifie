import antares
import numpy as np 
import os

output_dir = './'
geom_dir = './'

# get coordinates and connectivity
x, y = np.genfromtxt(os.path.join(geom_dir, 'coordinates.dat'), unpack=True)
#
co = np.genfromtxt(os.path.join(geom_dir, 'connectivity.dat'))
co -= 1

# get variables
p = []
with open(os.path.join(geom_dir, 'p1.dat'), 'r') as fid:
	counter = -1
	for line in fid:
		counter += 1
		if counter == 0:
			continue
		numbers = [float(n) for n in line.split()]
		for n in numbers:
			p.append(n)
p = np.array(p)
#
ur = []
with open(os.path.join(geom_dir, 'ur1.dat'), 'r') as fid:
	counter = -1
	for line in fid:
		counter += 1
		if counter == 0:
			continue
		numbers = [float(n) for n in line.split()]
		for n in numbers:
			ur.append(n)
ur = np.array(ur)
#
ux = []
with open(os.path.join(geom_dir, 'ux1.dat'), 'r') as fid:
	counter = -1
	for line in fid:
		counter += 1
		if counter == 0:
			continue
		numbers = [float(n) for n in line.split()]
		for n in numbers:
			ux.append(n)
ux = np.array(ux)
#
ut = np.zeros(ux.shape)

# generate an antares base with this
base = antares.Base()
base.init()
base[0][0]['x'] = x
base[0][0]['y'] = y
base[0][0]['z'] = np.zeros(x.shape)
base[0][0]['ur'] = ur
base[0][0]['ut'] = ut
base[0][0]['ux'] = ux
base[0][0].connectivity['tri'] = co

# output the base
writer = antares.Writer('bin_tp')
writer['base'] = base
writer['filename'] = os.path.join(geom_dir, 'sphere_flow.plt')
writer.dump()

