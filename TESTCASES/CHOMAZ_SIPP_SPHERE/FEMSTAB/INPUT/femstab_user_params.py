# ---------------------------------------
# All parameters to a biglobal analysis
# ---------------------------------------

# baseflow file location
basename = 'BASE/sol_timeazimave.plt'

# location of the stability matrices
linloc = 'LIN'

# location of outputs and post-processing
pploc = 'POSTPROCESS'

# ---------------------------------------

boundaries = {'wall':{'x':None,'y':None,'z':None,
					  'rho':'neumann',
					  'ux':'dirichlet',
					  'uy':'dirichlet',
					  'uz':'dirichlet',
					  'T':'dirichlet',
					  'label':1}}

# boundaries = {'inlet':{'x':0,'y':None,'z':None,
# 					   'ux':'dirichlet',
# 					   'uy':'dirichlet',
# 					   'p':'neumann',
# 					   'label':1},
# 			  'top_wall':{'x':None,'y':2,'z':None,
# 			  			  'ux':'dirichlet',
# 			  			  'uy':'dirichlet',
# 			  			  'p':'neumann',
# 			  			  'label':2},
# 			  'axis':{'x':None,'y':0,'z':None,
# 			  			  'ux':'dirichlet',
# 			  			  'uy':'neumann',
# 			  			  'p':'dirichlet',
# 			  			  'label':4},
# 			  'outlet':{'x':80,'y':None,'z':None,
# 			  			  'ux':'neumann',
# 			  			  'uy':'neumann',
# 			  			  'p':'neumann',
# 			  			  'label':3}}

# ---------------------------------------

# average kinematic viscosity
# visc_law can be
## None
## sutherland
## power
visco_law = None
mu_ref = 2.23442e-04
T_ref = 300.0
coeff = 110.6
rho_ref = 1.13798
visco_turb = False

# For compressible flows only now
gamma = 1.4
Cp = 1005.2
Pr = 0.71

# linearized equations to be solved, among:
## axi_incompressible_NS
## incompressible_NS_2d
## incompressible_NS_3d
## compressible_NS_3d
equations = 'axi_incompressible_NS'

# ---------------------------------------

# azimuthal wavenumber
## axi_incompressible_NS : True
## incompressible_NS_2d : False
## incompressible_NS_3d : False
## compressible_NS_3d : False
axisymmetric = True
mmin = 29
mmax = 29
mstep = 1

# depth wavenumber in the Z direction in case of 3D
## axi_incompressible_NS : False
## incompressible_NS_2d : False
## incompressible_NS_3d : True
## compressible_NS_3d : True
is3d = True
kmin = 0
kmax = 10
kstep = 1

# ---------------------------------------

# mesh refinement
refinement = False
ratio = 1.6
hmax = 7.0e-03

# number of proc for PETSc/SLEPc solution
mpiprocs = 1

# targets for PETSc/SLEPc solution process
# WARNING :: omega in RAD/S
om_min = 1000
om_max = 5000
om_num = 10
nev = 20
ncv = 60

# ---------------------------------------

verbosity = 10
