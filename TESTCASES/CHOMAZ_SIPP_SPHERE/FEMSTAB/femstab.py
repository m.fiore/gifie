#!/usr/bin/env python
# ---------------------------------------
# Load modules and required packages
# ---------------------------------------
#
# modules
import imp
import os
import sys
from create_matrices import *
#
# local package for user parameters
argFile = 'femstab_user_params.py'
if not os.path.isfile(argFile):
	print ' Fatal error : user parameters file %s not found.'%argFile
	exit()
args = imp.load_source('user_param', argFile)

# ---------------------------------------
# Add a few things to the arguments
# ---------------------------------------
#
args.__name__ = argFile
#
if not args.visco_law or args.visco_law == 'constant':
	try:
		args.nu = args.mu_ref / args.rho_ref
	except:
		print 'Fatal error: without a ''visco_law'' or for constant ''visco_law'', ''mu_ref'' and ''rho_ref'' are mandatory arguments'
		exit()
else:
	args.nu = 1.0
#
if not hasattr(args, 'boundaries') or (hasattr(args, 'boundaries') and len(args.boundaries) == 0):
	args.boundaries = None

# ---------------------------------------
# Prepare the working directory if need be
# ---------------------------------------
#
if not os.path.isdir(args.linloc):
	os.mkdir(args.linloc)
#
if not os.path.isdir(args.pploc):
	os.mkdir(args.pploc)

# ---------------------------------------
# Read in the extra command line arguments
# ---------------------------------------
#
modeonly = False
if len(sys.argv) > 1:
    for idx, arg in enumerate(sys.argv):
        if '--modeonly' in arg:
            modeonly = True

# build the matrices if and only if they don't already exist of course
if not modeonly:
	# ---------------------------------------
	# Transform the baseflow into FFem format
	# ---------------------------------------
	#
	# Prepare the object
	cm_handle = CreateMatrices(args.basename, args.equations)
	# Load the base flow
	cm_handle.read_baseflow()
	# Write the new mesh
	prefix, meshname, suffix = cm_handle.write_ffem_mesh(boundaries=args.boundaries)
	# Write the solutions
	#- the velocities
	outputs = cm_handle.write_ffem_velocities(prefix=prefix, suffix=suffix)
	#- export the viscosity field
	viscodynname, viscokinname = cm_handle.write_ffem_viscosity(args.nu,
											   visco_turb=args.visco_turb,
											   visco_law=args.visco_law,
											   mu_ref=args.mu_ref,
											   T_ref=args.T_ref,
											   coeff=args.coeff,
											   prefix=prefix,
											   suffix=outputs[-1])
	#- the compressible thingies
	comp_outputs = cm_handle.write_ffem_compvars(args.gamma, args.Cp, args.Pr, args.visco_law, args.T_ref, args.mu_ref, args.coeff, prefix=prefix, suffix=outputs[-1])	
	#
	# ---------------------------------------
	# Call FFem to build the matrices
	# ---------------------------------------
	#
	if cm_handle.axi:
		urname, utname, uxname, suffix = outputs
		ffem_arguments = ' -mesh %s -ur %s -ut %s -ux %s -nu %s'%(meshname, urname, utname, uxname, viscokinname)
		ffem_arguments += ' -prefix %s -suffix %s -lin %s'%(prefix, suffix, args.linloc)
		ffem_arguments += ' -refine %d -ratio %f -hmax %f'%(int(args.refinement), args.ratio, args.hmax)
		ffem_arguments += ' -nol %d'%(cm_handle.nol)
		os.system('%s -v %d %s %s'%(cm_handle.ffem_location, args.verbosity, cm_handle.ffem_file, ffem_arguments))
	elif cm_handle.is2d:
		uxname, uyname, suffix = outputs
		ffem_arguments = ' -mesh %s -ux %s -uy %s -nu %s'%(meshname, uxname, uyname, viscokinname)
		ffem_arguments += ' -refine %d -ratio %f -hmax %f'%(int(args.refinement), args.ratio, args.hmax)
		ffem_arguments += ' -prefix %s -suffix %s -lin %s'%(prefix, suffix, args.linloc)
		ffem_arguments += ' -nol %d'%(cm_handle.nol)		
		os.system('%s -v %d %s %s'%(cm_handle.ffem_location, args.verbosity, cm_handle.ffem_file, ffem_arguments))
	elif cm_handle.is3d:
		uxname, uyname, uzname, suffix = outputs
		if cm_handle.isComp:
			ffem_arguments = ' -mesh %s -ux %s -uy %s -uz %s -mu %s'%(meshname, uxname, uyname, uzname, viscodynname)
			roname, tname, pname, gamname, cpname, kapname, dmdtname, d2mdt2name, dkdtname, d2kdt2name, _ = comp_outputs
			ffem_arguments += ' -ro %s -T %s -p %s -gam %s -cp %s -kap %s -dmdt %s -d2mdt2 %s -dkdt %s -d2kdt2 %s'%(
											  roname, tname, pname,
											  gamname, cpname, kapname, 
											  dmdtname, d2mdt2name, 
											  dkdtname, d2kdt2name)
		else:
			ffem_arguments = ' -mesh %s -ux %s -uy %s -uz %s -nu %s'%(meshname, uxname, uyname, uzname, viscokinname)
		ffem_arguments += ' -refine %d -ratio %f -hmax %f'%(int(args.refinement), args.ratio, args.hmax)
		ffem_arguments += ' -prefix %s -suffix %s -lin %s'%(prefix, suffix, args.linloc)
		ffem_arguments += ' -nol %d'%(cm_handle.nol)		
		os.system('%s -v %d %s %s'%(cm_handle.ffem_location, args.verbosity, cm_handle.ffem_file, ffem_arguments))
	else:
		print 'Fatal error: the set of equations you asked for is not handled yet.'
		exit()
#
# else go directly for the solver
#
# ---------------------------------------
# Call the PETSc/SLEPc solver
# ---------------------------------------
#
if args.is3d:
	if args.kmax > args.kmin:
		vecK = np.arange(int(args.kmin), int(args.kmax)+1, int(args.kstep))
	else:
		vecK = np.array([args.kmin])
	counter = -1
elif args.axisymmetric:
	if args.mmax > args.mmin:
		vecM = np.arange(int(args.mmin), int(args.mmax)+1, int(args.mstep))
	else:
		vecM = np.array([args.mmin])
	counter = -1
#
while True:
	if args.is3d:
		counter += 1
		if counter >= len(vecK):
			break
		cK = vecK[counter]
	elif args.axisymmetric:
		counter += 1
		if counter >= len(vecM):
			break
		cM = vecM[counter]
	#
	petsc_arguments = ''
	if modeonly:
		petsc_arguments += '--modeonly'
	if args.axisymmetric:
		petsc_arguments += ' --axisymmetric'
		petsc_arguments += ' --m=%f'%cM
	if args.is3d:
		petsc_arguments += ' --3d'
		petsc_arguments += ' --k=%f'%cK
	#
	if args.mpiprocs > 1:
		os.system('mpirun -np %d compute_modes.py %s'%(args.mpiprocs, petsc_arguments))
	else:
		os.system('compute_modes.py %s'%petsc_arguments)
	#
	if args.is3d:
		try:
			os.mkdir('%s/K%d'%(args.pploc, cK))
		except OSError:
			pass
		os.rename('%s/spectrum.dat'%args.linloc, '%s/K%d/spectrum.dat'%(args.pploc, cK))
		os.rename('%s/base_p1.plt'%args.pploc, '%s/K%d/base_p1.plt'%(args.pploc, cK))
		os.rename('%s/base_p2.plt'%args.pploc, '%s/K%d/base_p2.plt'%(args.pploc, cK))
		modeonly = True		
	elif args.axisymmetric:
		try:
			os.mkdir('%s/M%d'%(args.pploc, cM))
		except OSError:
			pass
		os.rename('%s/spectrum.dat'%args.linloc, '%s/M%d/spectrum.dat'%(args.pploc, cM))
		os.rename('%s/base_p1.plt'%args.pploc, '%s/M%d/base_p1.plt'%(args.pploc, cM))
		os.rename('%s/base_p2.plt'%args.pploc, '%s/M%d/base_p2.plt'%(args.pploc, cM))
		modeonly = True
	else:
		break

# ---------------------------------------
# Conclude
# ---------------------------------------
#
exit()
