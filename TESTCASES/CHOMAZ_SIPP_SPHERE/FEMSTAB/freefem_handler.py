import os
import copy as cp_mod
from numpy import *

import scipy.sparse as sp

import matplotlib.tri as tri

class FreeFEMdisc():
    """
    object that contains all information about the FreeFem++
    discretization
    
    GENERAL INFORMATION

    ndof           : integer, nomber of degrees of freedom
    ntot           : integer, total number of discretization elements
                     (including those that are 0 due to BCs)
    newind         : integer array (ntot), contain the new index of a 
                     point when only DOFs are kept, or -1 if a dirichlet
                     BC is applied at that point
    nvar           : integer, nomber of variables (i.e. ux, uy, p,...)
    n              : integer array (nvar): number of DOFs in each variable 
    n0             : integer array (nvar): number of elements in each variable 
    np1            : integer, number of elements on the P1 mesh
    np2            : integer, number of elements on the P2 mesh

    idof    [ivar] : integer array (ndof), indicating which DOFs correspond
                     to variable 'ivar'
    idofi   [ivar] : integer array (ntot), indicating which element correspond
                     to variable 'ivar'
    itot    [ivar] : integer array (np1 or np2), indicating the correspondance 
                     between elements and DOFs of variable ivar
    vartype [ivar] : string array, indicating if the field is discretized 
                     P1 or P2 elements
    varorder[ivar] : order or the elements of variable 'ivar' relative to the
                     corresponding mesh

    MESHES

    meshp1         : matplotlib.tri.Triangulation, P1 mesh
    meshp2         : matplotlib.tri.Triangulation, P2 mesh

    MATRICES (containing anly DOFs)

    L              : real or complex scipy.sparse CSR matrix, 
    B              : real or complex scipy.sparse CSR matrix, mass matrix
 
    q0             : real array, base flow state vector

    """

    def __init__(self, di, axisymmetric=False, azim=0, threeD=False, k=0):
        """
        Initilize the object using either 
          - the text files written by FreeFem++ in folder 'di' (the 
            base flow data should be in 'di'/../base/ unless given
            in dibase)
          - an *.h5 file obtained using function SaveHDF5
        """

        if di[-3:] == '.h5':
            self.LoadHDF5(di, axisymmetric=axisymmetric, azim=azim, threeD=threeD, k=k)
        else:
            self.LoadDatFiles(di, axisymmetric=axisymmetric, azim=azim, threeD=threeD, k=k)

    def LoadDatFiles(self, di, axisymmetric=False, azim=0, threeD=False, k=0):

        ls = os.listdir(di)
        dibase = di + '../BASE/'
        
        # Find out which components of the state vector correspond to Dirichlet BCs
        if 'BC.dat' in ls:
            self.BCmat, self.newind = self.LoadBC(di+'/BC.dat')
            self.ndof = len(nonzero(self.newind != -1)[0])
        else:
            raise IOError("Cannot find BC.dat in "+repr(di))

        # Read Matrices
        # Left Hand Side
        if 'LNS-I.dat' in ls and 'LNS-J.dat' in ls and 'LNS-C.dat' in ls:
            icoo = fromfile(di+'/LNS-I.dat', dtype=int)[1:]
            jcoo = fromfile(di+'/LNS-J.dat', dtype=int)[1:]
            fC = open(di+'/LNS-C.dat', 'rb')
            fC.seek(8,0)
            dcoo = fromfile(fC, dtype=complex)
            fC.close()
            ijcoo = [icoo,jcoo]
            self.mat = sp.csc_matrix((dcoo,ijcoo))
            if axisymmetric:
                self.matM0 = sp.csc_matrix((dcoo,ijcoo))
            if threeD:
                self.matK0 = sp.csc_matrix((dcoo,ijcoo))
            del dcoo,ijcoo,icoo,jcoo
        else:
            raise IOError("Cannot find LNS-{I,J,C}.dat in "+repr(di))
        #
        if axisymmetric:
            if 'LNSM-I.dat' in ls and 'LNSM-J.dat' in ls and 'LNSM-C.dat' in ls:
                icoo = fromfile(di+'/LNSM-I.dat', dtype=int)[1:]
                jcoo = fromfile(di+'/LNSM-J.dat', dtype=int)[1:]
                fC = open(di+'/LNSM-C.dat', 'rb')
                fC.seek(8,0)
                dcoo = fromfile(fC, dtype=complex)
                fC.close()
                ijcoo = [icoo,jcoo]
                self.matM = sp.csc_matrix((dcoo,ijcoo))
                del dcoo,ijcoo,icoo,jcoo
                self.mat = self.mat + self.matM * azim
            else:
                raise IOError("Cannot find LNSM-{I,J,C}.dat in "+repr(di))
            #
            if 'LNSM2-I.dat' in ls and 'LNSM2-J.dat' in ls and 'LNSM2-C.dat' in ls:
                icoo = fromfile(di+'/LNSM2-I.dat', dtype=int)[1:]
                jcoo = fromfile(di+'/LNSM2-J.dat', dtype=int)[1:]
                fC = open(di+'/LNSM2-C.dat', 'rb')
                fC.seek(8,0)
                dcoo = fromfile(fC, dtype=complex)
                fC.close()
                ijcoo = [icoo,jcoo]
                self.matM2 = sp.csc_matrix((dcoo,ijcoo))
                del dcoo,ijcoo,icoo,jcoo
                self.mat = self.mat + self.matM2 * azim * azim
            else:
                raise IOError("Cannot find LNSM2-{I,J,C}.dat in "+repr(di))
        #
        if threeD:
            if 'LNSK-I.dat' in ls and 'LNSK-J.dat' in ls and 'LNSK-C.dat' in ls:
                icoo = fromfile(di+'/LNSK-I.dat', dtype=int)[1:]
                jcoo = fromfile(di+'/LNSK-J.dat', dtype=int)[1:]
                fC = open(di+'/LNSK-C.dat', 'rb')
                fC.seek(8,0)
                dcoo = fromfile(fC, dtype=complex)
                fC.close()
                ijcoo = [icoo,jcoo]
                self.matK = sp.csc_matrix((dcoo,ijcoo))
                del dcoo,ijcoo,icoo,jcoo
                self.mat = self.mat + self.matK * k
            else:
                raise IOError("Cannot find LNSK-{I,J,C}.dat in "+repr(di))
            #
            if 'LNSK2-I.dat' in ls and 'LNSK2-J.dat' in ls and 'LNSK2-C.dat' in ls:
                icoo = fromfile(di+'/LNSK2-I.dat', dtype=int)[1:]
                jcoo = fromfile(di+'/LNSK2-J.dat', dtype=int)[1:]
                fC = open(di+'/LNSK2-C.dat', 'rb')
                fC.seek(8,0)
                dcoo = fromfile(fC, dtype=complex)
                fC.close()
                ijcoo = [icoo,jcoo]
                self.matK2 = sp.csc_matrix((dcoo,ijcoo))
                del dcoo,ijcoo,icoo,jcoo
                self.mat = self.mat + self.matK2 * k * k
            else:
                raise IOError("Cannot find LNSK2-{I,J,C}.dat in "+repr(di))
        #
        self.L = self.BCmat.transpose() * self.mat * self.BCmat

        # Right Hand Side
        if 'B-I.dat' in ls and 'B-J.dat' in ls and 'B-C.dat' in ls:
            icoo = fromfile(di+'/B-I.dat', dtype=int)[1:]
            jcoo = fromfile(di+'/B-J.dat', dtype=int)[1:]
            fC = open(di+'/B-C.dat', 'rb')
            fC.seek(8,0)
            dcoo = fromfile(fC, dtype=complex)
            fC.close()
            ijcoo = [icoo,jcoo]
            self.mat = sp.csc_matrix((dcoo,ijcoo))
            del dcoo,ijcoo,icoo,jcoo
        else:
            raise IOError("Cannot find B-{I,J,C}.dat in "+repr(di))
        #
        self.B = self.BCmat.transpose() * self.mat * self.BCmat

        # Find the number of variables
        print ''
        tmp = loadtxt(di+'/dofs.dat')
        nvar = int(tmp[:,0].max() + 1)
        print 'Number of variables : ',nvar
        self.nvar = nvar
        self.ntot = len(tmp[:,0])
        
        self.LoadVars(tmp)        

        # Meshes
        try:
            meshtri1,meshpts1 = self.LoadMesh(dibase+'/ffem-connectivity.dat',dibase+'/ffem-coordinates.dat')
        except IOError:
            raise IOError('Cannot find '+ dibase+'/ffem-connectivity.dat and '+dibase+'/ffem-coordinates.dat')

        try:
            meshtri2,meshpts2 = self.LoadMesh(dibase+'/ffem-connectivity-2.dat',dibase+'/ffem-coordinates-2.dat')
        except IOError:
            raise IOError('Cannot find '+ dibase+'/ffem-connectivity-2.dat and '+dibase+'/ffem-coordinates-2.dat')

        self.np1 = len(meshpts1[:,0])
        self.np2 = len(meshpts2[:,0])

        self.vartype=[]
        for i in range(self.nvar):
            if self.n0[i] == self.np1:
                self.vartype.append('p1')
                print '  Variable # %2d : %6d ndof. Type: %s'%(i,self.n[i],'p1')
            elif self.n0[i] == self.np2:
                self.vartype.append('p2')
                print '  Variable # %2d : %6d ndof. Type: %s'%(i,self.n[i],'p2')
            else:
                print self.n0[i], self.np1,self.np2,self.n[i]
                raise ValueError('Neither P1 nor P2')

        self.meshp1 = tri.Triangulation(meshpts1[:,0],meshpts1[:,1],meshtri1)
        xyp1 = []
        for i in range(self.np1):
            xyp1.append((meshpts1[i,0],meshpts1[i,1]))
        xyp1 = array(xyp1,dtype=[('x', 'float'), ('y', 'float')])
        self.meshp2 = tri.Triangulation(meshpts2[:,0],meshpts2[:,1],meshtri2)
        xyp2 = []
        for i in range(self.np2):
            xyp2.append((meshpts2[i,0],meshpts2[i,1]))
        xyp2 = array(xyp2,dtype=[('x', 'float'), ('y', 'float')])

        # Associate DOFs with mesh points
        self.varorder=[]
        for i in range(self.nvar):
              indi = argsort(self.xydof[i],order=['x','y'])
              if self.vartype[i] == 'p1':
                  indm = argsort(xyp1,order=['x','y'])
              elif self.vartype[i] == 'p2':
                  indm = argsort(xyp2,order=['x','y'])
              
              ii   = argsort(indi)
              iii  = ii[indm]
              iiii = argsort(indi[iii])
              self.varorder.append(indi[iiii])

        # Load base flow
        self.q0 = loadtxt(di+'/base.dat')

    def SaveHDF5(self, fname, axisymmetric=False, threeD=False):
        """
        Save the FreeFEMdisc object using HDF5 in file 'fname'. 
        It can be loaded when initializing an object
        """

        def savemath5(f,mat,gname):
            grp  = f.create_group(gname)
            mat  = mat.tocsr()
            dset = grp.create_dataset('shape'     ,data=mat.shape   )
            dset = grp.create_dataset('indices'   ,data=mat.indices )
            dset = grp.create_dataset('indptr'    ,data=mat.indptr  )
            dset = grp.create_dataset('data'      ,data=mat.data    )

        def savemesth5(f,msh,gname):
            grp  = f.create_group(gname)
            dset = grp.create_dataset('x'         ,data=msh.x         )
            dset = grp.create_dataset('y'         ,data=msh.y         )
            dset = grp.create_dataset('triangles' ,data=msh.triangles )
        
        import h5py as h5
        os.system('rm -f '+fname)
        file=h5.File(fname)
        # save general information
        grpgen = file.create_group('general')
        dset = grpgen.create_dataset('ndof'     ,data=self.ndof     )  
        dset = grpgen.create_dataset('ntot'     ,data=self.ntot     )
        dset = grpgen.create_dataset('newind'   ,data=self.newind   )
        dset = grpgen.create_dataset('nvar'     ,data=self.nvar     )
        dset = grpgen.create_dataset('n'        ,data=self.n        )
        dset = grpgen.create_dataset('n0'       ,data=self.n0       )
        dset = grpgen.create_dataset('np1'      ,data=self.np1      )
        dset = grpgen.create_dataset('np2'      ,data=self.np2      ) 
        
        # save dof information
        grpdof = file.create_group('dof')
        for ivar in range(self.nvar):
            grp  = grpdof.create_group('dof_%d'%ivar)
            dset = grp.create_dataset('idof'     ,data=self.idof    [ivar])
            dset = grp.create_dataset('idofi'    ,data=self.idofi   [ivar])
            dset = grp.create_dataset('itot'     ,data=self.itot    [ivar])
            dset = grp.create_dataset('vartype'  ,data=self.vartype [ivar])
            dset = grp.create_dataset('varorder' ,data=self.varorder[ivar])
            dset = grp.create_dataset('xydof'    ,data=self.xydof   [ivar])

        # save meshes
        savemesth5(file, self.meshp1, 'meshp1')
        savemesth5(file, self.meshp2, 'meshp2')

        # save matrices
        if axisymmetric:
            savemath5(file, self.BCmat, 'BCmat')
            savemath5(file, self.matM0, 'matM0')
            savemath5(file, self.matM, 'matM')
            savemath5(file, self.matM2, 'matM2')
        elif threeD:
            savemath5(file, self.BCmat, 'BCmat')
            savemath5(file, self.matK0, 'matK0')
            savemath5(file, self.matK, 'matK')
            savemath5(file, self.matK2, 'matK2')            
        else:
            savemath5(file, self.L ,'L')
        #
        savemath5(file, self.B, 'B')
        # save base flow
        grpbase = file.create_group('base')
        dset = grpbase.create_dataset('q0', data=self.q0)
        file.close()

    def LoadHDF5(self, fname, axisymmetric=False, azim=0, threeD=False, k=0):

        def loadmath5(f, gname):
            shape   = f[gname+'/shape'  ].value
            indices = f[gname+'/indices'].value
            indptr  = f[gname+'/indptr' ].value
            data    = f[gname+'/data'   ].value
            return sp.csr_matrix((data, indices, indptr), shape=(shape[0], shape[1]))

        def loadmesth5(f, gname):
            x         = f[gname+'/x'        ].value
            y         = f[gname+'/y'        ].value
            triangles = f[gname+'/triangles'].value
            return tri.Triangulation(x,y,triangles)

        import h5py as h5
        file=h5.File(fname,'r')
        # load general information
        self.ndof      = file['general/ndof'     ].value
        self.ntot      = file['general/ntot'     ].value
        self.newind    = file['general/newind'   ].value
        self.nvar      = file['general/nvar'     ].value
        self.n         = file['general/n'        ].value
        self.n0        = file['general/n0'       ].value
        self.np1       = file['general/np1'      ].value
        self.np2       = file['general/np2'      ].value 

        # load dof information
        self.idof     = []
        self.idofi    = []
        self.itot     = []
        self.vartype  = []
        self.varorder = []
        self.xydof    = []

        for ivar in range(self.nvar):
            self.idof     .append(file['dof/dof_%d/idof'    %ivar].value)
            self.idofi    .append(file['dof/dof_%d/idofi'   %ivar].value)
            self.itot     .append(file['dof/dof_%d/itot'    %ivar].value)
            self.vartype  .append(file['dof/dof_%d/vartype' %ivar].value)
            self.varorder .append(file['dof/dof_%d/varorder'%ivar].value)
            self.xydof    .append(file['dof/dof_%d/xydof'   %ivar].value)

        # load meshes
        self.meshp1 = loadmesth5(file,'meshp1')
        self.meshp2 = loadmesth5(file,'meshp2')

        # load matrices
        if axisymmetric:
            self.BCmat = loadmath5(file, 'BCmat')
            self.matM0 = loadmath5(file, 'matM0')
            self.matM = loadmath5(file, 'matM')
            self.matM2 = loadmath5(file, 'matM2')
            mat = self.matM0 + self.matM * azim + self.matM2 * azim * azim
            self.L = self.BCmat.transpose() * mat * self.BCmat
        elif threeD:
            self.BCmat = loadmath5(file, 'BCmat')
            self.matK0 = loadmath5(file, 'matK0')
            self.matK = loadmath5(file, 'matK')
            self.matK2 = loadmath5(file, 'matK2')
            mat = self.matK0 + self.matK * azim + self.matK2 * azim * azim
            self.L = self.BCmat.transpose() * mat * self.BCmat            
        else:
            self.L  = loadmath5(file,'L')
        #
        self.B  = loadmath5(file,'B')
        # load base flow
        self.q0 = file['base/q0'].value

        file.close()

    def LoadBC(self,name):
        tmp=loadtxt(name)
        n=size(tmp)
        ind=0
        ival=[]
        jval=[]
        dval=[]
        new_ind=zeros(n,'int')
        for i in range(n):
            if abs(tmp[i]) < 1e-10:
                ival.append(i)
                jval.append(ind)
                dval.append(1.)
                new_ind[i]=ind
                ind+=1
            else:
                new_ind[i]=-1
        dcoo=array(dval,'complex')
        ijcoo=[array(ival,'int'),array(jval,'int')]

        # Create COO matrix
        mat=sp.coo_matrix((dcoo,ijcoo),shape=(n,ind))

        # Convert to CSR format
        mat=mat.tocsc()

        return mat,new_ind

    def LoadVars(self,tmp):
        
        idof  = []
        itot  = []
        idofi = []
        ind   = []
        xydof = []
        for i in range(self.nvar):
            idof .append([])
            itot .append([])
            idofi.append([])
            xydof.append([])
            ind  .append(0)

        # Fill the lists
        for i in range(self.ntot):
            ii = int(tmp[i,0])
            if self.newind[i]!=-1:
                idof[ii].append(self.newind[i])
                itot[ii].append(ind[ii])
            idofi[ii].append(i)
            ind[ii]+=1
            xydof[ii].append((tmp[i,1],tmp[i,2]))
        
        # Convert lists to arays
        self.n         = []
        self.n0        = []
        
        for i in range(self.nvar):
            idof [i] = array(idof [i],'int')
            itot [i] = array(itot [i],'int')
            idofi[i] = array(idofi[i],'int')
            xydof[i] = array(xydof[i],dtype=[('x', 'float'), ('y', 'float')])
            self.n.append (len(idof [i]))
            self.n0.append(len(idofi[i]))
            
        self.n     = array(self.n)
        self.n0    = array(self.n0)

        assert (self.ndof == sum(self.n))
        self.idof  = idof 
        self.itot  = itot 
        self.idofi = idofi
        self.xydof = xydof

    def LoadMesh(self,triname,ptsname):

        triangles  = loadtxt(triname)
        triangles -= 1
        pts        = loadtxt(ptsname)
        return triangles,pts

class EmptyFreeFEMdisc():
    def __init__(self):
        self.L = None
        self.B = None


