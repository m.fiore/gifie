#!/usr/bin/env python

# ---------------------------------------
# Parse pythonic arguments first
# ---------------------------------------
import numpy as np
import sys
# save the list of argument and deal first with program inputs
# delete them if found so slepc has a correct list of options
arglist = sys.argv
modeonly = False
c_axisymmetric = False
c_azim = 0
c_3d = False
c_k = 0
if len(arglist) > 1:
    todel=[]
    for idx, arg in enumerate(arglist):
        if '--modeonly' in arg:
            modeonly = True
            todel.append(idx)
        elif '--axisymmetric' in arg:
            c_axisymmetric = True
            todel.append(idx)
        elif '--m' in arg:
            c_azim = float(arg.split('=')[1])
            todel.append(idx)
        elif '--3d' in arg:
            c_3d = True
            todel.append(idx)
        elif '--k' in arg:
            c_k = float(arg.split('=')[1])
            todel.append(idx)
    arglist = np.delete(np.asarray(arglist), todel).tolist()

# ---------------------------------------
# Load modules and required packages
# ---------------------------------------
import os
import imp
#
import h5py
#
import petsc4py
import slepc4py
slepc4py.init(arglist)
from petsc4py import PETSc
from slepc4py import SLEPc
from mpi4py import MPI
#
import freefem_handler as ff
import parallel_solver as pfs
from parallel_solver import Print, PrintRed, PrintGreen
#
# local package for user parameters
argFile = 'femstab_user_params.py'
if not os.path.isfile(argFile):
    print ' Fatal error : user parameters file %s not found.'%argFile
    exit()
args = imp.load_source('user_param', argFile)

# ---------------------------------------
# Add some options to PETSc for solving
# ---------------------------------------
opts = PETSc.Options()
opts.setValue('st_ksp_type','preonly')
opts.setValue('st_pc_type','lu')
opts.setValue('st_pc_factor_mat_solver_package','mumps')

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if not rank:
    if not modeonly:
        if os.path.isfile('%s/ffdata.h5'%args.linloc):
            os.system('rm %s/ffdata.h5'%args.linloc)
comm.Barrier()

if not os.path.isfile('%s/ffdata.h5'%args.linloc):
    # Build FreeFEMdisc from .dat files
    Print('Loading dat files ... ')
    if rank == 0:
        ffdisc = ff.FreeFEMdisc('%s/'%args.linloc, axisymmetric=c_axisymmetric, azim=c_azim, threeD=c_3d, k=c_k)
    else:
        ffdisc = ff.EmptyFreeFEMdisc()
    PrintGreen('done \n')

    # Save as HDF5
    Print('Saving as .h5 file ... ')
    if rank == 0:
        ffdisc.SaveHDF5('%s/ffdata.h5'%args.linloc, axisymmetric=c_axisymmetric, threeD=c_3d)
    PrintGreen('done \n')
else:
    # Loading from .h5 file
    Print('Loading from .h5 file ... ')
    if rank == 0:
        ffdisc = ff.FreeFEMdisc('%s/ffdata.h5'%args.linloc, axisymmetric=c_axisymmetric, azim=c_azim, threeD=c_3d, k=c_k)
    else:
        ffdisc = ff.EmptyFreeFEMdisc()
    PrintGreen('done \n')

# Create PETSC matrices
Print('Convert matrices to PETSC parallel format ... ')
Lmat = pfs.CSR2Mat(ffdisc.L)
Bmat = pfs.CSR2Mat(ffdisc.B)
PrintGreen('done \n')
# print np.any(np.isnan(ffdisc.L.todense())), np.any(np.isnan(ffdisc.B.todense()))
# print np.any(np.isinf(ffdisc.L.todense())), np.any(np.isinf(ffdisc.B.todense()))
del ffdisc.L,ffdisc.B

# Compute modes using SLEPc
Print('Compute eigenmodes ... ')
if type(args.om_min) is complex:
    iomega = [args.om_min]
else:
    if args.om_max > args.om_min:
        iomega = np.linspace(args.om_min, args.om_max, args.om_num)
    else:
        iomega = [args.om_min]

modes = []
omegas = []
for idx, omega0 in enumerate(iomega):
    shift = omega0
    Print(' shift : (%+10.4g,%+10.4g) '%(shift.real,shift.imag))
    nev = args.nev
    ncv = args.ncv

    t1 = MPI.Wtime()
    _omegas, _modes, _residuals = pfs.DirectModeSLEPc(Lmat, Bmat, shift, nev, ncv)
    t2 = MPI.Wtime()
    
    Print(' CPU time : %10.4g '%(t2-t1))
    if rank == 0:
        print _omegas.shape, '/', args.nev
        for om in _omegas:
            omegas.append(om)

        print _modes.shape
        for jdx in range(_modes.shape[1]):
            modes.append(_modes[:,jdx])

        for idx, om in enumerate(_omegas):
            print np.real(om), np.imag(om), _residuals[idx]

PrintGreen('done \n')

if rank == 0:

    omegas = np.squeeze(np.asarray(omegas))
    print omegas.shape
    print np.asarray(modes).shape
    modes = np.asarray(modes).T

    order = np.argsort(omegas.imag)[::-1]
    omegas = omegas[order]
    modes = modes[:, order]

    f = open('%s/spectrum.dat'%args.linloc,'w')
    for idx in xrange(omegas.size):
        om = omegas[idx]
        f.write('%+.16f %+.16f\n'%(np.real(om), np.imag(om)))
    f.close()

    import antares
    if args.equations != 'compressible_Euler_2d': #and args.equations != 'compressible_NS_3d':
        P2_base = antares.Base()
        P2_base.init()
        P2_base[0][0]['x'] = ffdisc.meshp2.x
        P2_base[0][0]['y'] = ffdisc.meshp2.y
        P2_base[0][0]['z'] = 0.0*P2_base[0][0]['y']
        P2_base[0][0].connectivity['tri'] = ffdisc.meshp2.triangles
        for idx in xrange(modes.shape[1]):
            if args.equations == 'compressible_NS_3d':
                names = ['rho', 'ux', 'uy', 'uz', 'T']
                loc = [1, 2, 3]
            elif args.axisymmetric:
                names = ['ur', 'ut', 'uz']
                loc = [0, 1, 2]
            elif args.is3d:
                names = ['ux', 'uy', 'uz']
                loc = [0, 1, 2]
            else:
                names = ['ux', 'uy']
                loc = [0, 1]
            for jdx in loc:
                v = np.zeros(ffdisc.np2,np.dtype(modes[0,idx]))
                if len(modes[:,idx]) == ffdisc.ndof:
                    qui             = modes[:,idx][ffdisc.idof[jdx]]
                    v[ffdisc.itot[jdx]] = qui
                    v               = v[ffdisc.varorder[jdx]]
                elif len(modes[:,idx]) == ffdisc.ntot:
                    v               = modes[:,idx][ffdisc.idofi[jdx]]
                    v               = v[ffdisc.varorder[jdx]]
                P2_base[0][0][names[jdx]+'_%05d_re'%(idx+1)] = v.real
                P2_base[0][0][names[jdx]+'_%05d_im'%(idx+1)] = v.imag

        writer = antares.Writer('bin_tp')
        writer['base'] = P2_base
        writer['filename'] = '%s/base_p2.plt'%args.pploc
        writer.dump()

    P1_base = antares.Base()
    P1_base.init()
    P1_base[0][0]['x'] = ffdisc.meshp1.x
    P1_base[0][0]['y'] = ffdisc.meshp1.y
    P1_base[0][0]['z'] = 0.0*P1_base[0][0]['y']
    P1_base[0][0].connectivity['tri'] = ffdisc.meshp1.triangles
    for idx in xrange(modes.shape[1]):
        if args.equations == 'compressible_Euler_2d':
            loc = [0, 1, 2, 3]
            names = ['ux', 'uy', 'p', 'T']
        elif args.equations == 'compressible_NS_3d':
            # loc = [0,1,2,3,4]
            names = ['rho', 'ux', 'uy', 'uz', 'T']
            loc = [0, 4]
            # names = ['rho', 'T']
        elif args.axisymmetric:
            loc = [3]
            names = ['ur', 'ut', 'uz', 'p']
        elif args.is3d:
            loc = [3]
            names = ['ux','uy','uz','p']
        else:
            loc = [2]
            names = ['ux','uy','p']
        for jdx in loc:
            v = np.zeros(ffdisc.np1,np.dtype(modes[0,idx]))
            if len(modes[:,idx]) == ffdisc.ndof:
                qui             = modes[:,idx][ffdisc.idof[jdx]]
                v[ffdisc.itot[jdx]] = qui
                v               = v[ffdisc.varorder[jdx]]
            elif len(modes[:,idx]) == ffdisc.ntot:
                v               = modes[:,idx][ffdisc.idofi[jdx]]
                v               = v[ffdisc.varorder[jdx]]
            P1_base[0][0][names[jdx]+'_%05d_re'%(idx+1)] = v.real
            P1_base[0][0][names[jdx]+'_%05d_im'%(idx+1)] = v.imag

    writer = antares.Writer('bin_tp')
    writer['base'] = P1_base
    writer['filename'] = '%s/base_p1.plt'%args.pploc
    writer.dump()    

exit()
