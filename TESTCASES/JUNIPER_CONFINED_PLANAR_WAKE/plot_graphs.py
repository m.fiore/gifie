from antares import *
import os
import numpy as np

import matplotlib as mpl
# mpl.use('Agg')
font={'family':'serif','weight':'medium','size':40}
mpl.rc('font',**font)
mpl.rc('text', usetex=True)
mpl.rcParams['axes.linewidth'] = 5.0
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import scipy.io as scio

#------------------------------------------------

ppdir = 'POSTPROCESS'
topdirs = ['H1', 'IL1-2']
subdirs = ['PERFECT_SLIP', 'NO_SLIP']
files = [['baseflownoBL_h1_il-'+il+'.plt' for il in ['1.15', '1.25', '1.3', '1.35']],
		 ['baseflowwithBL_h1_il-'+il+'.plt' for il in ['1', '1.15', '1.2', '1.25', '1.3', '1.35']],
		 ['baseflownoBL_h'+h+'_il-1.2.plt' for h in ['1.22', '1.35', '1.5', '1.67', '1.86', '2.33', '4', '9', '10']],
		 ['baseflowwithBL_h'+h+'_il-1.2.plt' for h in ['1', '1.5', '2.33', '4', '9', '10']]]
abscissas = [[-1.15, -1.25, -1.3, -1.35], 
			 [-1., -1.15, -1.2, -1.25, -1.3, -1.35], 
			 [1.22, 1.35, 1.5, 1.67, 1.86, 2.33, 4., 9., 10.],
			 [1., 1.5, 2.33, 4., 9., 10.]]
xlabels = [r'1/$\Lambda$', r'1/$\Lambda$', r'h', r'h']
legend_pos = [['upper left', 'upper right'], ['upper left', 'lower right'], ['upper right', 'lower right'], ['best', 'upper right']]
matthewdata = [['./POSTPROCESS/matthewdata090831/firstmodenoBL_Re100_h1_il-'+il+'.mat' for il in ['1.15', '1.25', '1.3', '1.35']],
			   ['./POSTPROCESS/matthewdata090831/firstmodewithBL_Re100_h1_il-'+il+'.mat' for il in ['1', '1.15', '1.2', '1.25', '1.3', '1.35']],
			   ['./POSTPROCESS/matthewdata090831/firstmodenoBL_Re100_h'+h+'_il-1.2.mat' for h in ['1.22', '1.35', '1.5', '1.67', '1.86', '2.33', '4', '9', '10']],
			   ['./POSTPROCESS/matthewdata090831/firstmodewithBL_Re100_h'+h+'_il-1.2.mat' for h in ['1', '1.5', '2.33', '4', '9', '10']]]

#------------------------------------------------

counter = -1
for idx, topdir in enumerate(topdirs):
	for jdx, subdir in enumerate(subdirs):
		kdx = jdx + 2*idx
		counter += 1
		sz = len(files[kdx])
		maxima = np.zeros((sz, 2))
		maxima_matthew = np.zeros((sz, 2))
		#
		for ldx, id in enumerate(files[kdx]):
			dirname = os.path.join(ppdir, topdir, subdir, id)
			c_freq, c_ampli = np.genfromtxt(os.path.join(dirname, 'spectrum.dat'), unpack=True)
			mask = c_freq > 0
			imax = c_ampli[mask].argmax()
			maxima[ldx,0], maxima[ldx,1] = c_freq[mask][imax], c_ampli[mask][imax]
			#
			matdata = scio.loadmat(matthewdata[kdx][ldx])
			maxima_matthew[ldx, 0], maxima_matthew[ldx, 1] = matdata['firsteig'][0][0].real, matdata['firsteig'][0][0].imag
			matthew_x = matdata['bildxmat']
			matthew_y = matdata['bildymat']
			matthew_vy = matdata['vmat']
			matthew_vy = matthew_vy / matthew_vy.max()
			#
			reader = Reader('bin_tp')
			reader['filename'] = os.path.join(dirname, 'base_p2.plt')
			evecbase = reader.read()
			evecinst = evecbase[0][0]
			del reader
			#
			vecY = evecinst['y'].squeeze()
			vecX = evecinst['x'].squeeze()
			modenumber = np.arange(c_ampli.size)[mask][imax]
			uy = evecinst['uy_%05d_re'%(modenumber+1)]
			uy /= uy.max()
			#
			xirange = vecX.max() - vecX.min()
			yirange = vecY.max() - vecY.min()
			dirname = os.path.join(ppdir, topdir, subdir)
			#
			fig = plt.figure(figsize=(60, 15))#,dpi=150)
			ax = plt.subplot(211)
			im = ax.tricontourf(vecX,vecY,evecinst.connectivity['tri'],uy,np.linspace(uy.min(),1.0,101),
						     	  cmap=cm.seismic,origin='lower',extent=(vecX.min(),vecX.max(),vecY.min(),vecY.max()), vmin=-1.0, vmax=1.0)
			cb = plt.colorbar(im,ax=ax,shrink=0.6,pad=0.01)
			cb.set_ticks([-1.0,-0.5,0.0,0.5,1.0])
			cb.ax.tick_params(labelsize=30)
			levels = [-1.0,-0.8,-0.6,-0.4,-0.2,0.2,0.4,0.6,0.8,1.0]
			ax.tricontour(vecX,vecY,evecinst.connectivity['tri'],uy,levels,linewidths=3.0,colors='k')
			ax.set_ylabel(r'Y')
			ax.set_xlabel(r'X',labelpad=20)
			ax.axis('equal')
			ax.set_adjustable('box-forced')
			ax.set_ylim(0., 2.)
			ax.set_yticks([0., 1., 2.])
			ax.set_yticklabels(['0', '1', '2'])
			ax.set_xlim(0., 20.)
			ax.set_xticks([0., 2., 4., 6., 8., 10., 12., 14., 16., 18.])
			ax.set_xticklabels(['0', '2', '4', '6', '8', '10', '12', '14', '16', '18'])
			ax.tick_params(which='major',width=3,length=20,direction='in')
			ax.annotate('GIFIE',(1.4,1.6))
			#
			ax = plt.subplot(212)
			im = ax.contourf(matthew_x,matthew_y,matthew_vy,np.linspace(-1.0,1.0,101),
						     	  cmap=cm.seismic,origin='lower',extent=(matthew_x.min(),matthew_x.max(),matthew_y.min(),matthew_y.max()),vmin=-1.0,vmax=1.0)
			cb = plt.colorbar(im,ax=ax,shrink=0.6,pad=0.01)
			cb.set_ticks([-1.0,-0.5,0.0,0.5,1.0])
			cb.ax.tick_params(labelsize=30)
			levels = [-1.0,-0.8,-0.6,-0.4,-0.2,0.2,0.4,0.6,0.8,1.0]
			ax.contour(matthew_x,matthew_y,matthew_vy,levels,linewidths=3.0,colors='k')
			ax.set_ylabel(r'Y')
			ax.set_xlabel(r'X',labelpad=20)
			ax.axis('equal')
			ax.set_adjustable('box-forced')
			ax.set_ylim(0., 2.)
			ax.set_yticks([0., 1., 2.])
			ax.set_yticklabels(['0', '1', '2'])
			ax.set_xlim(0., 20.)
			ax.set_xticks([0., 2., 4., 6., 8., 10., 12., 14., 16., 18.])
			ax.set_xticklabels(['0', '2', '4', '6', '8', '10', '12', '14', '16', '18'])
			ax.tick_params(which='major',width=3,length=20,direction='in')
			ax.annotate('Juniper et al. (2011)',(1.4,1.6))
			#
			plt.subplots_adjust(hspace=0.0001)
			plt.savefig(dirname+'/uy_%05d.pdf'%(ldx+1),bbox_inches='tight')
			plt.close()
		#
		dirname = os.path.join(ppdir, topdir, subdir)
		fig = plt.figure(figsize=(20,20))#, dpi=200)
		ax = plt.subplot(111)
		ax.plot(abscissas[kdx], maxima[:,1], '-ok', lw=4, ms=20, mew=4, mfc='None', mec='k', label='GIFIE')
		ax.plot(abscissas[kdx], maxima_matthew[:,1], '-sk', lw=4, ms=20, mew=4, mfc='None', mec='k', label='Juniper et al. (2011)')
		ax.set_ylabel(r'Im($\omega_G$)')
		ax.set_xlabel(xlabels[kdx])
		if counter == 2:
			ax.set_xticks([1.22, 1.5, 2.33, 4, 9, 10])
			ax.set_xticklabels([str(lab) for lab in [1.2, 1.5, 2.33, 4, 9, 10]])
		else:
			ax.set_xticks(abscissas[kdx])
			ax.set_xticklabels([str(lab) for lab in abscissas[kdx]])
		ax.tick_params(which='major', width=3, length=20, direction='in')
		ax.legend(loc=legend_pos[kdx][0], shadow=True, fancybox=True)
		plt.savefig(dirname+'/lines_im_omegaG.pdf', bbox_inches='tight')
		plt.close()
		#
		fig = plt.figure(figsize=(20,20))#, dpi=200)
		ax = plt.subplot(111)
		ax.plot(abscissas[kdx], maxima[:,0], '-ok', lw=4, ms=20, mew=4, mfc='None', mec='k', label='GIFIE')
		ax.plot(abscissas[kdx], abs(maxima_matthew[:,0]), '-sk', lw=4, ms=20, mew=4, mfc='None', mec='k', label='Juniper et al. (2011)')
		ax.set_ylabel(r'Re($\omega_G$)')
		ax.set_xlabel(xlabels[kdx])
		if counter == 2:
			ax.set_xticks([1.22, 1.5, 2.33, 4, 9, 10])
			ax.set_xticklabels([str(lab) for lab in [1.2, 1.5, 2.33, 4, 9, 10]])
		else:
			ax.set_xticks(abscissas[kdx])
			ax.set_xticklabels([str(lab) for lab in abscissas[kdx]])		
		ax.tick_params(which='major', width=3, length=20, direction='in')
		ax.legend(loc=legend_pos[kdx][1], shadow=True, fancybox=True)
		plt.savefig(dirname+'/lines_re_omegaG.pdf', bbox_inches='tight')
		plt.close()

exit()
