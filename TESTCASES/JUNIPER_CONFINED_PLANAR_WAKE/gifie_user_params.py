# ---------------------------------------
# All parameters to a biglobal analysis
# ---------------------------------------

# baseflow file location
basename = './BASE/baseflowwithBL_h1_il-1.15.plt'

# location of the stability matrices
linloc = 'LIN'

# location of outputs and post-processing
pploc = 'POSTPROCESS'

# ---------------------------------------

boundaries = {'inlet':{'x':'xmin','y':None,'z':None,
					   'ux':'dirichlet',
					   'uy':'dirichlet',
					   'p':'neumann',
					   'label':4},
			  'top_wall':{'x':None,'y':'ymax','z':None,
			  			  'ux':'dirichlet',
			  			  'uy':'dirichlet',
			  			  'p':'neumann',
			  			  'label':3},
			  'low_wall':{'x':None,'y':'ymin','z':None,
			  			  'ux':'dirichlet',
			  			  'uy':'dirichlet',
			  			  'p':'neumann',
			  			  'label':1},
			  'outlet':{'x':'xmax','y':None,'z':None,
			  			  'ux':'neumann',
			  			  'uy':'neumann',
			  			  'p':'neumann',
			  			  'label':2}}

# ---------------------------------------

# average kinematic viscosity
# visc_law can be
## None
## sutherland
## power
visco_law = None
mu_ref = 0.01
T_ref = 300.0
coeff = 110.6
rho_ref = 1.0
visco_turb = False

# For compressible flows only now
gamma = 1.4
Cp = 1005.2
Pr = 0.71

# linearized equations to be solved, among:
## axi_incompressible_NS
## incompressible_NS_2d
## incompressible_NS_3d
## compressible_NS_3d
equations = 'incompressible_NS_2d'

# ---------------------------------------

# azimuthal wavenumber
## axi_incompressible_NS : True
## compressible_Euler_Poisson : False
## incompressible_NS_2d : False
## incompressible_NS_3d : False
axisymmetric = False
mmin = 29
mmax = 29
mstep = 1

# depth wavenumber in the Z direction in case of 3D
## axi_incompressible_NS : False
## compressible_Euler_Poisson : False
## incompressible_NS_2d : False
## incompressible_NS_3d : True
is3d = False
kmin = 0
kmax = 10
kstep = 1

# ---------------------------------------

# mesh refinement
refinement = True
ratio = 1.1
hmax = 0.2

# number of proc for PETSc/SLEPc solution
mpiprocs = 2

# targets for PETSc/SLEPc solution process
# WARNING :: omega in RAD/S
om_min = 1.0
om_max = 1.0
om_num = 10
nev = 30
ncv = 60

# ---------------------------------------

verbosity = 1
