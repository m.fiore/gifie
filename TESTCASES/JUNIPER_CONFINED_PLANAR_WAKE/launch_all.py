import os

ppdir = 'POSTPROCESS'
topdirs = ['H1', 'IL1-2']
subdirs = ['PERFECT_SLIP', 'NO_SLIP']
files = [['baseflownoBL_h1_il-'+il+'.plt' for il in ['1.15', '1.25', '1.3', '1.35']],
		 ['baseflowwithBL_h1_il-'+il+'.plt' for il in ['1', '1.15', '1.2', '1.25', '1.3', '1.35']],
		 ['baseflownoBL_h'+h+'_il-1.2.plt' for h in ['1.22', '1.35', '1.5', '1.67', '1.86', '2.33', '4', '9', '10']],
		 ['baseflowwithBL_h'+h+'_il-1.2.plt' for h in ['1', '1.5', '2.33', '4', '9', '10']]]

for idx, topdir in enumerate(topdirs):
	for jdx, subdir in enumerate(subdirs):
		kdx = jdx + 2*idx
		for ldx, baseflowname in enumerate(files[kdx]):
			#
			fid = './BASE/'+baseflowname
			os.system('gsed -i "s@basename = .*@basename = \'%s\'@g" gifie_user_params.py'%fid)
			#
			os.system('gifie.py')
			#
			dirname = ppdir+'/'+topdir+'/'+subdir+'/'+baseflowname
			if os.path.isdir(dirname):
				os.system('rm -r %s'%dirname)
			os.mkdir(dirname)
			os.system('mv LIN/spectrum.dat %s/.'%dirname)
			os.system('mv POSTPROCESS/base_p1.plt %s/.'%dirname)
			os.system('mv POSTPROCESS/base_p2.plt %s/.'%dirname)

exit()
