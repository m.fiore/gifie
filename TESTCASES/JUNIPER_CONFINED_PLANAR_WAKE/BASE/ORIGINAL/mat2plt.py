import antares as ant
import matplotlib as mpl
mpl.use('Agg')
font={'family':'serif','weight':'medium','size':40}
mpl.rc('font',**font)
mpl.rcParams['axes.linewidth'] = 5.0
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import scipy.io as scio
import os
from slicer import slice_2d_base

# ---------------------------------------

vars = ['xmatbf', 'ymatbf', 'Umat', 'Vmat']

lof = os.popen('ls *.mat').read().split('\n')[:-1]

for idx, fid in enumerate(lof):
	#
	outname = fid[:-4] + '_half.plt'
	outfig = fid[:-4] + '.png'
	#
	dico = scio.loadmat(fid)
	x, y, ux, uy = [dico[key] for key in vars]
	#
	fig = plt.figure(figsize=(30,20), dpi=50)
	ax = plt.subplot(211)
	im = plt.contourf(x, y, ux, 20, cmap=cm.jet)
	plt.contour(x, y, ux, 20, colors='k', linewidths=2)
	cbi = plt.colorbar(im)
	ax.set_xlim(0,80.)
	ax.set_ylim(0,2.)
	ax.axis('auto')
	ax.tick_params(which='major',width=3,length=20)
	ax = plt.subplot(212)
	im = plt.contourf(x, y, uy, 20, cmap=cm.jet)
	plt.contour(x, y, uy, 20, colors='k', linewidths=2)
	cbi = plt.colorbar(im)
	ax.set_xlim(0,80.)
	ax.set_ylim(0,2.)
	ax.axis('auto')
	ax.tick_params(which='major',width=3,length=20)
	plt.savefig(outfig, bbox_inches='tight')
	plt.close()
	#
	vecX = x[0,:]
	vecY = y[:,0]
	newVecY = np.concatenate((-vecY[::-1], vecY[1:]))
	newX = np.vstack((x, x[1:,:]))
	newY = np.vstack((-y[::-1,:], y[1:,:]))
	newUx = np.concatenate((ux[::-1, :], ux[1:,:]))
	newUy = np.concatenate((-uy[::-1,:], uy[1:,:]))
	#
	base = ant.Base()
	base.init()
	base[0][0]['x'] = x #newX
	base[0][0]['y'] = y #newY
	base[0][0]['ux'] = ux #newUx
	base[0][0]['uy'] = uy #newUy
	#
	base.unstructure()
	base = slice_2d_base(base)
	#
	# check connectivity for negative volumes
	x, y = base[0][0]['x'], base[0][0]['y']
	v1, v2, v3 = base[0][0].connectivity['tri'][:,0], base[0][0].connectivity['tri'][:,1], base[0][0].connectivity['tri'][:,2]
	bax, bay, cax, cay = x[v2]-x[v1], y[v2]-y[v1], x[v3]-x[v1], y[v3]-y[v1]
	det = bax*cay - bay*cax
	mask = det < 0
	base[0][0].connectivity['tri'][mask, 1], base[0][0].connectivity['tri'][mask, 2] = \
	base[0][0].connectivity['tri'][mask ,2], base[0][0].connectivity['tri'][mask, 1]
	#
	writer = ant.Writer('bin_tp')
	writer['base'] = base
	writer['filename'] = outname
	writer.dump()

exit()
