# ---------------------------------------
# Load modules and external packages
# ---------------------------------------
# Reference
# "How to subdivide pyramids, prisms and hexaedra into tetrahedra"
# J. Dompierre, P. Labbe, M-G. Vallet, R. Camarero
# Rapport CERCA R99-78, 24 august 1999
# Conference paper from the 8th International Meshing Roundtable, Lake Tahoe, Cal., 10-13/10/1999

import numpy as np
import copy

# ---------------------------------------
# Custom functions
# ---------------------------------------

def slice_base(base):
	#
	if base.is_structured() or base.is_structured() is None:
		base.unstructure()
	#
	for zone in base.keys():
		if base[zone].shared.shape is not None:
			in_co = base[zone].shared.connectivity
			new_co = __slicer(in_co)
			#
			keys = copy.copy(base[zone].shared.connectivity.keys())
			for key in keys:
				del base[zone].shared.connectivity[key]
			base[zone].shared.connectivity['tet'] = new_co
		else:
			for instant in base[zone].keys():
				in_co = base[zone][instant].connectivity
				new_co = __slicer(in_co)
				#
				keys = copy.copy(base[zone][instant].connectivity.keys())
				for key in keys:
					del base[zone][instant].connectivity[key]
				base[zone][instant].connectivity['tet'] = new_co
	#
	return base

def slice_2d_base(base):
	#
	if base.is_structured() or base.is_structured() is None:
		base.unstructure()
	#
	for zone in base.keys():
		if base[zone].shared.shape is not None:
			in_co = base[zone].shared.connectivity
			new_co = __slicer_2d(in_co)
			#
			keys = copy.copy(base[zone].shared.connectivity.keys())
			for key in keys:
				del base[zone].shared.connectivity[key]
			base[zone].shared.connectivity['tri'] = new_co
		else:
			for instant in base[zone].keys():
				in_co = base[zone][instant].connectivity
				new_co = __slicer_2d(in_co)
				#
				keys = copy.copy(base[zone][instant].connectivity.keys())
				for key in keys:
					del base[zone][instant].connectivity[key]
				base[zone][instant].connectivity['tri'] = new_co
	#
	return base	

def __slicer_2d(co):
	#
	qua2tri = None
	tri = None
	#
	for key in co.keys():
		if key == 'qua':
			qua2tri = __slice_qua(co[key])
		elif key == 'tri':
			tri = co[key]
		else:
			print 'Fatal error : element type %s not handled by the slicer.'%key
			exit()
	#
	if qua2tri is not None:
		out_co = qua2tri
	#
	if tri is not None:
		out_co = np.vstack((out_co, tri))
	#
	return out_co

def __slicer(co):
	#
	pyr2tet = None
	pri2tet = None
	hex2tet = None
	tet = None
	#
	for key in co.keys():
		if key == 'pyr':
			pyr2tet = __slice_pyr(co[key])
		elif key == 'pri':
			pri2tet = __slice_pri(co[key])
		elif key == 'hex':
			hex2tet = __slice_hex(co[key])
		elif key == 'tet':
			tet = co[key]
		else:
			print 'Fatal error : element type %s not handled by the slicer'%key
			exit()
	#
	if pyr2tet is None and pri2tet is None:
		out_co = hex2tet
	elif pyr2tet is None and hex2tet is None:
		out_co = pri2tet
	elif pri2tet is None and hex2tet is None:
		out_co = pyr2tet
	elif pyr2tet is None:
		out_co = np.vstack((pri2tet, hex2tet))
	elif pri2tet is None:
		out_co = np.vstack((pyr2tet, hex2tet))
	elif hex2tet is None:
		out_co = np.vstack((pyr2tet, pri2tet))
	else:
		out_co = np.vstack((pyr2tet, pri2tet, hex2tet))
	#
	if tet is not None:
		out_co = np.vstack((out_co, tet))
	#
	return out_co

def __slice_qua(in_co):
	#
	out_co = np.zeros((2,3), dtype=int)
	#
	criteria = np.amin(in_co[:, [0,2]], axis=1) - np.amin(in_co[:, [1,3]], axis=1)
	#
	mask = criteria < 0
	out_co = np.append(out_co, in_co[mask, :][:, [0, 1, 2]], axis=0)
	out_co = np.append(out_co, in_co[mask, :][:, [0, 2, 3]], axis=0)
	#
	mask = criteria > 0
	out_co = np.append(out_co, in_co[mask, :][:, [1, 2, 3]], axis=0)
	out_co = np.append(out_co, in_co[mask, :][:, [1, 3, 0]], axis=0)
	#
	return out_co[2:, :]

def __slice_pyr(in_co):
	#
	out_co = np.zeros((2,4), dtype=int)
	#
	criteria = np.amin(in_co[:, [0,2]], axis=1) - np.amin(in_co[:, [1,3]], axis=1)
	#
	mask = criteria < 0
	out_co = np.append(out_co, in_co[mask, :][:, [0, 1, 2, 4]], axis=0)
	out_co = np.append(out_co, in_co[mask, :][:, [0, 2, 3, 4]], axis=0)
	#
	mask = criteria > 0
	out_co = np.append(out_co, in_co[mask, :][:, [1, 2, 3, 4]], axis=0)
	out_co = np.append(out_co, in_co[mask, :][:, [1, 3, 0, 4]], axis=0)
	#
	return out_co[2:, :]

def __slice_pri(in_co):
	#
	indirections = np.array([[0,1,2,3,4,5],
							 [1,2,0,4,5,3],
							 [2,0,1,5,3,4],
							 [3,5,4,0,2,1],
							 [4,3,5,1,0,2],
							 [5,4,3,2,1,0]])
	#
	out_co = np.zeros((2,4), dtype=int)
	#
	smallest_vtx = np.argmin(in_co, axis=1)
	vertices = np.take(in_co, indirections[smallest_vtx, :] + in_co.shape[1]*np.arange(in_co.shape[0])[:,None])
	#
	criteria = np.amin(vertices[:, [1,5]], axis=1) - np.amin(vertices[:, [2,4]], axis=1)
	#
	mask = criteria < 0
	out_co = np.append(out_co, vertices[mask, :][:, [0, 1, 2, 5]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 1, 5, 4]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 4, 5, 3]], axis=0)
	#
	mask = criteria > 0
	out_co = np.append(out_co, vertices[mask, :][:, [0, 1, 2, 4]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 4, 2, 5]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 4, 5, 3]], axis=0)
	#
	return out_co[2:,:]

def __slice_hex(in_co):
	#
	indirections = np.array([[0,1,2,3,4,5,6,7],
							 [1,0,4,5,2,3,7,6],
							 [2,1,5,6,3,0,4,7],
							 [3,0,1,2,7,4,5,6],
							 [4,0,3,7,5,1,2,6],
							 [5,1,0,4,6,2,3,7],
							 [6,2,1,5,7,3,0,4],
							 [7,3,2,6,4,0,1,5]])
	#
	rotations = np.array([0, 120, 240, 0, 0, 240, 120, 0], dtype=int)
	#
	out_co = np.zeros((2,4), dtype=int)
	#
	smallest_vtx = np.argmin(in_co, axis=1)
	vertices = np.take(in_co, indirections[smallest_vtx, :] + in_co.shape[1]*np.arange(in_co.shape[0])[:,None])
	#
	bits = np.transpose(np.array([np.amin(vertices[:, [1,6]], axis=1) < np.amin(vertices[:, [2,5]], axis=1),
					 			  np.amin(vertices[:, [3,6]], axis=1) < np.amin(vertices[:, [2,7]], axis=1),
					 			  np.amin(vertices[:, [4,6]], axis=1) < np.amin(vertices[:, [5,7]], axis=1)]))
	num_of_diag_thru_v7 = (bits != 0).sum(1)
	#
	def b2i(a):
		return ((((((0 << 1)|a[0])<<1)|a[1])<<1)|a[2])
	bit2int = np.apply_along_axis(b2i, 1, bits)
	angles = rotations[bit2int]
	#
	mask = angles == 120
	# vertices[mask, :][:, [1, 4, 3, 5, 7, 2]] = vertices[mask, :][:, [4, 3, 1, 7, 2, 5]]
	vertices[mask, 1], vertices[mask, 4], vertices[mask, 3], vertices[mask, 5], vertices[mask, 7], vertices[mask, 2] = \
	vertices[mask, 4], vertices[mask, 3], vertices[mask, 1], vertices[mask, 7], vertices[mask, 2], vertices[mask, 5]
	#
	mask = angles == 240
	# vertices[mask, :][:, [1, 3, 4, 5, 2, 7]] = vertices[mask, :][:, [3, 4, 1, 2, 7, 5]]
	vertices[mask, 1], vertices[mask, 3], vertices[mask, 4], vertices[mask, 5], vertices[mask, 2], vertices[mask, 7] = \
	vertices[mask, 3], vertices[mask, 4], vertices[mask, 1], vertices[mask, 2], vertices[mask, 7], vertices[mask, 5]
	#
	mask = num_of_diag_thru_v7 == 0
	out_co = np.append(out_co, vertices[mask, :][:, [0, 1, 2, 5]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 2, 7, 5]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 2, 3, 7]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 5, 7, 4]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [2, 7, 6, 5]], axis=0)
	#
	mask = num_of_diag_thru_v7 == 1
	out_co = np.append(out_co, vertices[mask, :][:, [0, 5, 7, 4]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 1, 7, 5]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [1, 6, 7, 5]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 7, 2, 3]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 7, 1, 2]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [1, 7, 6, 2]], axis=0)
	#
	mask = num_of_diag_thru_v7 == 2
	out_co = np.append(out_co, vertices[mask, :][:, [0, 4, 5, 6]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 3, 7, 6]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 7, 4, 6]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 1, 2, 5]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 3, 6, 2]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 6, 5, 2]], axis=0)
	#
	mask = num_of_diag_thru_v7 == 3
	out_co = np.append(out_co, vertices[mask, :][:, [0, 2, 3, 6]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 3, 7, 6]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 7, 4, 6]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [0, 5, 6, 4]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [1, 5, 6, 0]], axis=0)
	out_co = np.append(out_co, vertices[mask, :][:, [1, 6, 2, 0]], axis=0)	
	#
	return out_co[2:,:]

