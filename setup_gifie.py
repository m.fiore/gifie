#!/usr/bin/env python

import sys
import os

RED = '\033[91m'
ITALIC = '\033[3m'
BOLD = '\033[1m'
BLINK = '\033[5m'
ENDC = '\033[0m'

print(BOLD+' ')
print('|-----------------------------------------------|')
print('|  FROM PYTHON V'+str(sys.version.split(' ')[0]))
print( '| ')
print( '|  WELCOME ON THE SETUP SCRIPT FOR')
print( '|    __________________________ ')
print( '|   / ____/  _/ ____/  _/ ____/ ')
print( '|  / / __ / // /_   / // __/    ')
print( '| / /_/ // // __/ _/ // /___    ')
print( '| \____/___/_/   /___/_____/    ' )                    
print( '| ')
print( '| A Global stabIlity Finite Elements SOLVER')
print( '|-----------------------------------------------|')
print( ' '+ENDC)

pwd = os.getcwd()
homedir = os.path.expanduser('~')

if os.path.isfile(os.path.join(homedir,'.bashrc')):
	flag = os.path.join(homedir,'.bashrc')
elif os.path.isfile(os.path.join(homedir,'.bash_profile')):
	flag = os.path.join(homedir,'.bash_profile')
elif os.path.isfile(os.path.join(homedir,'.cshrc')):
	flag = os.path.join(homedir,'.cshrc')
else:
	print (' ')
	print (RED+BOLD+BLINK+' WARNING '+ENDC)
	print (RED+BOLD+' SETUP FAILED '+ENDC)
	print (RED+BOLD+' REASON: FILE ".bashrc" OR ".bash_profile" OR .cshrc" NOT FOUND IN HOMEDIR "%s%s"'%(homedir,os.sep)+ENDC)
	flag = False

if flag:
	if '.bash' in flag:
		with open(flag,'a') as fid:
			fid.write('export GIFIE_HOME=%s\n'%pwd)
			fid.write('export PATH=$GIFIE_HOME:$GIFIE_HOME/SOURCES:$PATH\n')
	else:
		with open(flag,'a') as fid:
			fid.write('setenv GIFIE_HOME %s\n'%pwd)
			fid.write('setenv PATH $GIFIE_HOME\:$GIFIE_HOME/SOURCES\:$PATH\n')

	print (' ')
	print (' Please enter the following command in your terminal: "source %s"'%flag)

print (' ')
print (BOLD+' GOOD BYE ! '+ENDC)
print (' ')
