# GIFIE

GIFIE (Global stabIlity Finite Elements) is a 2D-biglobal Linear Stability Analysis (LSA) solver in python 3.X & Freefem++.  
It is designed for 2D base flows and can handle axisymmetric and plane base flows.  
It also allows for the resolution of the adjoint stability problem, thus yielding the sensitivity of a flow to fluctuations.
The basic code is coupled to a PETSc/SLEPc to solve the eigenproblems in parallel - the PETSc library has been shown to be scalable up to 10000 processors.

## Installation

An automatic script called 'setup_gifie.py' prepares the environement variables and the path.  
From the GIFIE directory, just launch "./setup_gifie.py"

### Dependencies

Recommended versions of the _mandatory_ dependencies are:

* [Python 3.X](www.python.org) >= 3.8. Recommended: 3.8.2
* [NumPy](www.numpy.org) >= 1.9. Recommended: 1.11.1
* [H5py](www.h5py.org) >= 2.5. Recommended: 2.6
* [MPI4Py](mpi4py.scipy.org) >= 1.3. Recommended: 1.3.1
* [PETSc4Py](https://pypi.python.org/pypi/petsc4py) >= 3.7. Recommended: 3.7.0
* [SLEPc4Py](https://pypi.python.org/pypi/slepc4py) >= 3.7. Recommendned: 3.7.0

If you experience any trouble, please contact the creator at tbridel[@]cerfacs.fr.

## Documentation

The documentation is a work in progress.

### Use 

Once the GIFIE environment has been setup, it is very easy to launch GIFIE.

* Just type `gifie.py`,

from the case directory.

## Examples & Tutorial

Examples will eventually be available.

## Issue tracker

None

## Development rules

Coding style

https://www.python.org/dev/peps/pep-0008

http://docs.python-guide.org/en/latest/writing/style/

## Credits

### Creator

Thibault Bridel-Bertomeu

### Developers

### Reporters

## License

CERFACS
